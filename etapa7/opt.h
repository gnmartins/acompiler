/*
 *  Universidade Federal do Rio Grande do Sul
 *  Instituto de Informática
 *  INF01147 - Compiladores (2017/2)
 *
 *  Gabriel Nunes Martins
 *  Leonardo Silva Rosa
 */

#ifndef OPT_H
#define OPT_H

#include "hash.h"
#include "ast.h"
#include "tac.h"

AST_NODE* optAst(AST_NODE* node);
AST_NODE* optConstantFolding(AST_NODE* node);


void optTac(TAC* list);
void optUnusedFunctions(TAC* list);
void optUnusedVariables(TAC* list);

#endif