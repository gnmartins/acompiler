/*
*  Universidade Federal do Rio Grande do Sul
*  Instituto de Informática
*  INF01147 - Compiladores (2017/2)
*
*  Gabriel Nunes Martins
*  Leonardo Silva Rosa
*/

#include "opt.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_DECL_FUNC 128
#define MAX_USED_FUNC 256

#define SYMBOL_OP_TYPE 278

enum OP_TYPE {
    OP_ADD,
	OP_SUB,
	OP_MUL,
	OP_DIV,
	OP_LT,
	OP_GT,
	OP_LE,
	OP_GE,
	OP_EQ,
	OP_NE,
	OP_AND,
	OP_OR,
};

int countUsage(char* symbol, TAC* afterDecl);
long getOpValue(AST_NODE* op1, AST_NODE* op2, int op);
AST_NODE* foldOp(AST_NODE* node, int op);
AST_NODE* deadCodeRemoval(AST_NODE* node);

long getLongFromText(char* text)
{
    if (text[0] == '\'') {
        return text[1];
    } else {
        return atoi(text);
    }
}

AST_NODE* optAst(AST_NODE* node) 
{
    if (!node) return NULL;
    
    // TODO: make OR work
    if (node->type == AST_OR) return node;

    for (int i = 0; i < MAX_SONS; ++i) {
        node->sons[i] = optAst(node->sons[i]);
    }
    
    switch (node->type) {
        
        case AST_ADD:
            return foldOp(node, OP_ADD);
        case AST_SUB:
            return foldOp(node, OP_SUB);
        case AST_MUL:
            return foldOp(node, OP_MUL);
        case AST_DIV:
            return foldOp(node, OP_DIV);

        case AST_LT:
            return foldOp(node, OP_LT);
        case AST_GT:
            return foldOp(node, OP_GT);
        case AST_LE:
            return foldOp(node, OP_LE);
        case AST_GE:
            return foldOp(node, OP_GE);
        case AST_EQ:
            return foldOp(node, OP_EQ);
        case AST_NE:
            return foldOp(node, OP_NE);
        case AST_AND:
            return foldOp(node, OP_AND);

        case AST_IF:
        case AST_IF_ELSE:
        case AST_WHILE:
            return deadCodeRemoval(node);

        default:
            return node;
    }

    return node;
}

AST_NODE* deadCodeRemoval(AST_NODE* node)
{
    // fprintf(stderr, "entering dead code removal\n");
    int value;
    AST_NODE* cond = node->sons[0];

    if (cond->type == AST_SYMBOL) {
        value = getLongFromText(cond->symbol->text);
        
        switch (node->type) {
            case AST_WHILE:
                if (value == 0) return NULL;

            case AST_IF: 
                if (value == 0) return NULL;

            case AST_IF_ELSE:
                if (value == 1) 
                    return node->sons[1];
                else 
                    return node->sons[2];
        }

    }
    return node;
}

AST_NODE* foldOp(AST_NODE* node, int op) 
{
    if (node->sons[0]->type == AST_SYMBOL && node->sons[1]->type == AST_SYMBOL) {   
        AST_NODE* op1 = node->sons[0];
        AST_NODE* op2 = node->sons[1];

        long value = getOpValue(op1, op2, op);
        char valueStr[256];
        sprintf(valueStr, "%ld", value);
        
        HASH_NODE* symbol = hashInsert(
            op1->symbol->type, // same type as the literal being folded
            valueStr,          // new calculated value
            op1->symbol->dataType, 
            op1->symbol->lineNumber);
            
        node = astCreate(AST_SYMBOL, node->lineNumber, symbol, 
            NULL, NULL, NULL, NULL); // no sons, literal value
            
        // fprintf(stderr, "result %d %d (ops %s %s): %s\n", 
        //     op1->symbol->type,
        //     op1->symbol->dataType,
        //     op1->symbol->text,
        //     op2->symbol->text,
        //     node->symbol->text);
    }

    else if (op == OP_AND || op == OP_OR) {
        AST_NODE* son0 = node->sons[0];
        AST_NODE* son1 = node->sons[1];
        long value;
        int arg;

        if (son0->type == AST_SYMBOL) {
            value = getLongFromText(son0->symbol->text);
            arg = 1;
        }
        else if (son1->type == AST_SYMBOL) {
            value = getLongFromText(son1->symbol->text);
            arg = 0;
        }
        else {
            return node;
        }

        // short-circuiting AND
        if (value == 0 && op == OP_AND) {
            char valueStr[256];
            sprintf(valueStr, "%ld", value);

            HASH_NODE* symbol = hashInsert(
                SYMBOL_OP_TYPE, // same type as the literal being folded
                valueStr,           // new calculated value
                DATATYPE_INTEGER, 
                node->lineNumber);

            node = astCreate(AST_SYMBOL, node->lineNumber, symbol, 
                NULL, NULL, NULL, NULL); // no sons, literal value
        }

        else {
            node = node->sons[arg];
        }
        return node;
    }

    return node;
}

long getOpValue(AST_NODE* op1, AST_NODE* op2, int op)
{
    long v1 = getLongFromText(op1->symbol->text);
    long v2 = getLongFromText(op2->symbol->text);
    switch (op) {
        case OP_ADD:
            return v1 + v2;  
        case OP_SUB:
            return v1 - v2;     
        case OP_MUL:
            return v1 * v2;   
        case OP_DIV:
            return v1 / v2;
        case OP_LT:
            return v1 < v2;
        case OP_GT:
            return v1 > v2;
        case OP_LE:
            return v1 <= v2;
        case OP_GE:
            return v1 >= v2;
        case OP_EQ:
            return v1 == v2;
        case OP_NE:
            return v1 != v2;
        case OP_AND:
            return v1 == v2;
        case OP_OR:
            return v1 || v2;

        default:
            return 0;
    }
}

void optTac(TAC* list) 
{
    optUnusedFunctions(list);
    optUnusedVariables(list);
}

void optUnusedFunctions(TAC* list)
{
    // TODO: create more elegant and optimized solution for this
    char* usedFuncList[MAX_USED_FUNC];
    int usedFunctions = 0;
    
    TAC* funcDecl[MAX_DECL_FUNC];
    int declaredFunctions = 0;
    
    // getting functions calls and function declarations
    for (TAC* tac = list; tac != NULL; tac = tac->next) {
        if (tac->type == TAC_CALL) {
            usedFuncList[usedFunctions] = tac->arg1->text;
            usedFunctions++;
            if (usedFunctions == MAX_USED_FUNC) break;
        }
        if (tac->type == TAC_FUNC_START) {
            funcDecl[declaredFunctions] = tac;
            declaredFunctions++;
            if (declaredFunctions == MAX_DECL_FUNC) break;
        }
    }
    
    // removing unused functions
    for (int i = 0; i < declaredFunctions; ++i) {
        TAC* func = funcDecl[i];
        if (strcmp(func->res->text, "main") == 0) continue;
        
        int usages = 0;
        for (int j = 0; j < usedFunctions; ++j) {
            if (strcmp(func->res->text, usedFuncList[j]) == 0) {
                usages++;
                break;
            }
        }
        if (usages == 0) {
            // TODO: functions declared inside another functions
            TAC* end = NULL;
            for (end = func->next; end->type != TAC_FUNC_END; end = end->next) {}
            
            if (func->prev != NULL) 
            func->prev->next = end->next;
            if (end->next != NULL)
            end->next->prev = func->prev;
        }
    }
}

void optUnusedVariables(TAC* list) 
{
    for (TAC* tac = list; tac != NULL; tac = tac->next) {
        if (tac->type == TAC_VAR || tac->type == TAC_ARR) {
            if (countUsage(tac->res->text, tac->next) == 0) {
                // fprintf(stderr, "removing unused variable `%s`...\n", tac->res->text);
                if (tac->type == TAC_VAR) {
                    // fprintf(stderr, "removing variable `%s`...\n", tac->res->text);
                    
                    if (tac->prev != NULL) 
                    tac->prev->next = tac->next;
                    if (tac->next != NULL)
                    tac->next->prev = tac->prev;
                    
                }
                // removing array initialization
                else {
                    TAC* end = NULL;
                    for (end = tac->next; end && end->type == TAC_ARR_DECL; end = end->next) {}
                    
                    if (tac->prev != NULL) 
                    tac->prev->next = end;
                    if (end != NULL)
                    end->prev = tac->prev;
                }
            }
        }
    }
}

int countUsage(char* symbol, TAC* afterDecl)
{
            int count = 0;
            // fprintf(stderr, "counting usages of %s...\n", symbol);
            
            for (TAC* curr = afterDecl; curr != NULL; curr = curr->next) {
                if (curr->type != TAC_VAR &&
                    curr->type != TAC_ARR &&
                    curr->type != TAC_SYMBOL) {
                        
                        if (curr->res != NULL)
                        if (strcmp(curr->res->text, symbol) == 0) {
                            return ++count;
                        }
                        
                        if (curr->arg1 != NULL)
                        if (strcmp(curr->arg1->text, symbol) == 0) {
                            return ++count;
                        }
                        
                        if (curr->arg2 != NULL)
                        if (strcmp(curr->arg2->text, symbol) == 0) {
                            return ++count;
                        }
                    }
                }
                return count;
            }