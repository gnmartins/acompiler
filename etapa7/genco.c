/*
*  Universidade Federal do Rio Grande do Sul
*  Instituto de Informática
*  INF01147 - Compiladores (2017/2)
*
*  Gabriel Nunes Martins
*  Leonardo Silva Rosa
*/

#include "genco.h"

int strCount = 0;
char* strings[MAX_STRINGS];

int boolOpCount = 0;

int argsPassed = 0;
char* argRegisters[4] = { "edi", "esi", "edx", "ecx" };

long getValue(char* text) 
{
    if (text[0] == '\'') {
        return text[1];
    } else {
        return atoi(text);
    }
}

// treating everything as long
void gencoVarDecl(TAC* tac) 
{
    long number = getValue(tac->arg1->text);
    fprintf(out, "\t.globl %s\n", tac->res->text);
    fprintf(out, "\t.data\n");    
    fprintf(out, "\t.align 4\n");
    fprintf(out, "\t.type %s, @object\n", tac->res->text);
    fprintf(out, "\t.size %s, 4\n", tac->res->text);
    fprintf(out, "%s:\n", tac->res->text);
    fprintf(out, "\t.long %ld\n", number);

}

void gencoArrDecl(TAC* tac)
{
    long size = getValue(tac->arg1->text) * 4;

    fprintf(out, "\t.globl %s\n", tac->res->text);
    fprintf(out, "\t.data\n");    
    fprintf(out, "\t.align 4\n");
    fprintf(out, "\t.type %s, @object\n", tac->res->text);
    fprintf(out, "\t.size %s, %ld\n", tac->res->text, size);
    fprintf(out, "%s:\n", tac->res->text);
}

void gencoArrInit(TAC* tac)
{
    fprintf(out, "\t.long %ld\n", getValue(tac->res->text));
}

void gencoLabel(TAC* tac)
{
    fprintf(out, "%s:\n", tac->res->text);
}

void gencoAdd(TAC* tac)
{
    // allocating temp
    fprintf(out, "\t.comm %s, 4, 4\n", tac->res->text);

    // arg1
    if (tac->arg1->type == SYMBOL_SCALAR)
        fprintf(out, "\tmovl %s(%%rip), %%eax\n", tac->arg1->text);
    else
        fprintf(out, "\tmovl $%ld, %%eax\n", getValue(tac->arg1->text));

    // + arg2
    if (tac->arg2->type == SYMBOL_SCALAR)
        fprintf(out, "\taddl %s(%%rip), %%eax\n", tac->arg2->text);
    else
        fprintf(out, "\taddl $%ld, %%eax\n", getValue(tac->arg2->text));

    fprintf(out, "\tmovl %%eax, %s(%%rip)\n", tac->res->text);
}

void gencoSub(TAC* tac)
{
    // allocating temp
    fprintf(out, "\t.comm %s, 4, 4\n", tac->res->text);

    // arg1
    if (tac->arg1->type == SYMBOL_SCALAR)
        fprintf(out, "\tmovl %s(%%rip), %%edx\n", tac->arg1->text);
    else
        fprintf(out, "\tmovl $%ld, %%edx\n", getValue(tac->arg1->text));

    // arg2
    if (tac->arg2->type == SYMBOL_SCALAR)
        fprintf(out, "\tmovl %s(%%rip), %%eax\n", tac->arg2->text);
    else
        fprintf(out, "\tmovl $%ld, %%eax\n", getValue(tac->arg2->text));

    // subtraction
    fprintf(out, "\tsubl %%eax, %%edx\n");
    fprintf(out, "\tmovl %%edx, %%eax\n");

    fprintf(out, "\tmovl %%eax, %s(%%rip)\n", tac->res->text);
}

void gencoMul(TAC* tac)
{
    // allocating temp
    fprintf(out, "\t.comm %s, 4, 4\n", tac->res->text);

    // arg1
    if (tac->arg1->type == SYMBOL_SCALAR)
        fprintf(out, "\tmovl %s(%%rip), %%eax\n", tac->arg1->text);
    else
        fprintf(out, "\tmovl $%ld, %%eax\n", getValue(tac->arg1->text));

    // * arg2
    if (tac->arg2->type == SYMBOL_SCALAR)
        fprintf(out, "\timull %s(%%rip), %%eax\n", tac->arg2->text);
    else
        fprintf(out, "\timull $%ld, %%eax\n", getValue(tac->arg2->text));

    fprintf(out, "\tmovl %%eax, %s(%%rip)\n", tac->res->text);
}

void gencoDiv(TAC* tac)
{
    // allocating temp
    fprintf(out, "\t.comm %s, 4, 4\n", tac->res->text);

    // arg1
    if (tac->arg1->type == SYMBOL_SCALAR)
        fprintf(out, "\tmovl %s(%%rip), %%eax\n", tac->arg1->text);
    else
        fprintf(out, "\tmovl $%ld, %%eax\n", getValue(tac->arg1->text));

    fprintf(out, "\tcltd\n");

    // / arg2
    if (tac->arg2->type == SYMBOL_SCALAR)
        fprintf(out, "\tidivl %s(%%rip)\n", tac->arg2->text);
    else {
        fprintf(out, "\t.comm _%s_arg2, 4, 4\n", tac->res->text);
        fprintf(out, "\tmovl $%ld, _%s_arg2(%%rip)\n", getValue(tac->arg2->text), tac->res->text);
        fprintf(out, "\tidivl _%s_arg2(%%rip)\n", tac->res->text);
    }

    fprintf(out, "\tmovl %%eax, %s(%%rip)\n", tac->res->text);
}

void gencoBoolComparison(TAC* tac)
{
    char jump[16];
    int fst_set, snd_set;

    fst_set = 0;
    snd_set = 1;
    switch (tac->type) {
        case TAC_LT:
            strcpy(jump, "jl");
            break;
        case TAC_GT:
            strcpy(jump, "jg");
            break;
        case TAC_LE:
            strcpy(jump, "jle");
            break;
        case TAC_GE:
            strcpy(jump, "jge");
            break;

    }

    fprintf(out, "\t.comm %s, 4, 4\n", tac->res->text);
    // arg1
    if (tac->arg1->type == SYMBOL_SCALAR)
        fprintf(out, "\tmovl %s(%%rip), %%eax\n", tac->arg1->text);
    else
        fprintf(out, "\tmovl $%ld, %%eax\n", getValue(tac->arg1->text));
    // + arg2
    if (tac->arg2->type == SYMBOL_SCALAR)
        fprintf(out, "\tcmp %s(%%rip), %%eax\n", tac->arg2->text);
    else
        fprintf(out, "\tcmp $%ld, %%eax\n", getValue(tac->arg2->text));

    fprintf(out, "\t%s _fstlc%d\n", jump, boolOpCount);  // FirST Label from Comparison
    fprintf(out, "\tmovl $%d, %s(%%rip)\n", fst_set, tac->res->text);
    fprintf(out, "\tjmp _sndlc%d\n", boolOpCount);       // SecoND Label from Comparison
    fprintf(out, "_fstlc%d:\n", boolOpCount);
    fprintf(out, "\tmovl $%d, %s(%%rip)\n", snd_set, tac->res->text);
    fprintf(out, "_sndlc%d:\n", boolOpCount);
    boolOpCount++;
}

void gencoEqNeq(TAC* tac)
{
    fprintf(out, "\t.comm %s, 4, 4\n", tac->res->text);

    // arg1
    if (tac->arg1->type == SYMBOL_SCALAR)
        fprintf(out, "\tmovl %s(%%rip), %%eax\n", tac->arg1->text);
    else
        fprintf(out, "\tmovl $%ld, %%eax\n", getValue(tac->arg1->text));
    // + arg2
    if (tac->arg2->type == SYMBOL_SCALAR)
        fprintf(out, "\tmovl %s(%%rip), %%edx\n", tac->arg2->text);
    else
        fprintf(out, "\tmovl $%ld, %%edx\n", getValue(tac->arg2->text));

    fprintf(out, "\tcmpl %%eax, %%edx\n");
    if (tac->type == TAC_EQ) 
        fprintf(out, "\tsete %%al\n");
    else 
        fprintf(out, "\tsetne %%al\n");
    fprintf(out, "\tmovzbl %%al, %%eax\n");

    fprintf(out, "\tmovl %%eax, %s(%%rip)\n", tac->res->text);
}

void gencoAnd(TAC* tac)
{
    // allocating temp
    fprintf(out, "\t.comm %s, 4, 4\n", tac->res->text);

    // arg1
    if (tac->arg1->type == SYMBOL_SCALAR)
        fprintf(out, "\tcmpl $0, %s(%%rip)\n", tac->arg1->text);
    else
        fprintf(out, "\tcmpl $0, $%ld\n", getValue(tac->arg1->text));

    fprintf(out, "\tje .ANDFALSE%d\n", boolOpCount);

    // arg2
    if (tac->arg2->type == SYMBOL_SCALAR)
        fprintf(out, "\tcmpl $0, %s(%%rip)\n", tac->arg2->text);
    else
        fprintf(out, "\tcmpl $0, $%ld\n", getValue(tac->arg2->text));

    fprintf(out, "\tje .ANDFALSE%d\n", boolOpCount);

    // Result true
    fprintf(out, "\tmovl $1, %%eax\n");
    fprintf(out, "\tjmp .UPDATEVALUE%d\n", boolOpCount);

    // Result false
    fprintf(out, ".ANDFALSE%d:\n", boolOpCount);
    fprintf(out, "\tmovl $0, %%eax\n");

    // Update value
    fprintf(out, ".UPDATEVALUE%d:\n", boolOpCount);
    fprintf(out, "\tmovl %%eax, %s(%%rip)\n", tac->res->text);

    boolOpCount++;
}

void gencoOr(TAC* tac)
{
    // allocating temp
    fprintf(out, "\t.comm %s, 4, 4\n", tac->res->text);

    // arg1
    if (tac->arg1->type == SYMBOL_SCALAR)
        fprintf(out, "\tcmpl $0, %s(%%rip)\n", tac->arg1->text);
    else
        fprintf(out, "\tcmpl $0, $%ld\n", getValue(tac->arg1->text));

    fprintf(out, "\tjne .ORTRUE%d\n", boolOpCount);

    // arg2
    if (tac->arg2->type == SYMBOL_SCALAR)
        fprintf(out, "\tcmpl $0, %s(%%rip)\n", tac->arg2->text);
    else
        fprintf(out, "\tcmpl $0, $%ld\n", getValue(tac->arg2->text));

    fprintf(out, "\tjne .ORTRUE%d\n", boolOpCount);

    // Result false
    fprintf(out, "\tmovl $0, %%eax\n");
    fprintf(out, "\tjmp .UPDATEVALUE%d\n", boolOpCount);

    // Result true
    fprintf(out, ".ORTRUE%d:\n", boolOpCount);
    fprintf(out, "\tmovl $1, %%eax\n");

    // Update value
    fprintf(out, ".UPDATEVALUE%d:\n", boolOpCount);
    fprintf(out, "\tmovl %%eax, %s(%%rip)\n", tac->res->text);

    boolOpCount++;
}

void gencoArrAccess(TAC* tac)
{
    // allocating temp
    fprintf(out, "\t.comm %s, 4, 4\n", tac->res->text);

    if (tac->arg2->type == SYMBOL_SCALAR) {
        fprintf(out, "\tmovl %s(%%rip), %%eax\n", tac->arg2->text);
    } else {
        fprintf(out, "\tmovl $%ld, %%eax\n", getValue(tac->arg2->text));
    }
        
    fprintf(out, "\tcltq\n");
    fprintf(out, "\tmovl %s(,%%rax,4), %%eax\n", tac->arg1->text);
    fprintf(out, "\tmovl %%eax, %s(%%rip)\n", tac->res->text);
}

void gencoMov(TAC* tac)
{
    if (tac->arg1->type == SYMBOL_SCALAR) {
        fprintf(out, "\tmovl %s(%%rip), %%eax\n", tac->arg1->text);
    } else {
        fprintf(out, "\tmovl $%ld, %%eax\n", getValue(tac->arg1->text));
    }
    fprintf(out, "\tmovl %%eax, %s(%%rip)\n", tac->res->text);
}

void gencoMovInd(TAC* tac)
{
    // index
    if (tac->arg1->type == SYMBOL_SCALAR) {
        fprintf(out, "\tmovl %s(%%rip), %%eax\n", tac->arg1->text);
    } else {
        fprintf(out, "\tmovl $%ld, %%eax\n", getValue(tac->arg1->text));
    }

    // value
    if (tac->arg2->type == SYMBOL_SCALAR) 
        fprintf(out, "\tmovl %s(%%rip), %%edx\n", tac->arg2->text);
    else 
        fprintf(out, "\tmovl $%ld, %%edx\n", getValue(tac->arg2->text));

    // attribution
    fprintf(out, "\tcltq\n");
    fprintf(out, "\tmovl %%edx, %s(,%%rax,4)\n", tac->res->text);
}

void gencoIfz(TAC* tac) 
{
    fprintf(out, "\tcmpl $0, %s(%%rip)\n", tac->arg1->text);
    fprintf(out, "\tje %s\n", tac->res->text);
}

void gencoJump(TAC* tac)
{
    fprintf(out, "\tjmp %s\n", tac->res->text);
}

void gencoRead(TAC* tac) 
{
    fprintf(out, "\tmovl $%s, %%edi\n", tac->res->text);
    fprintf(out, "\tcall gets\n");
    fprintf(out, "\tmovl $%s, %%edi\n", tac->res->text);
    fprintf(out, "\tcall atoi\n");
    fprintf(out, "\tmovl %%eax, %s(%%rip)\n", tac->res->text);
}

void gencoFuncStart(TAC* tac)
{
    fprintf(out, "\t.globl %s\n", tac->res->text);
    fprintf(out, "\t.type %s, @function\n", tac->res->text);
    fprintf(out, "%s:\n", tac->res->text);
    fprintf(out, "\t.cfi_startproc\n");
    fprintf(out, "\tpushq %%rbp\n");
    fprintf(out, "\tmovq %%rsp, %%rbp\n");      
    
    int cnt = 0;
    for (TAC* param = tac->next; param->type == TAC_PARAMETER; param = param->next) {
        fprintf(out, "\t.comm %s, 4, 4\n", param->res->text);
        fprintf(out, "\tmovl %%%s, %s(%%rip)\n",
            argRegisters[cnt], param->res->text);
        cnt++;
    }
}

void gencoFuncEnd(TAC* tac)
{
    fprintf(out, "\tpopq %%rbp\n");
    fprintf(out, "\tret\n");
    fprintf(out, "\t.cfi_endproc\n");
}

void gencoPrint(TAC* tac)
{
    int selectedStr = 0;
    // printing expression
    if (tac->res->dataType != DATATYPE_STRING) {
        if (tac->res->type == SYMBOL_SCALAR) {
            fprintf(out, "\tmovl %s(%%rip), %%esi\n", tac->res->text);
        } else {
            fprintf(out, "\tmovl $%ld, %%esi\n", getValue(tac->res->text));
        }

    } else { // printing string
        strings[strCount] = tac->res->text;
        strCount++;
        selectedStr = strCount;

        fprintf(out, "\tmovl %%eax, %%esi\n");
    }

    fprintf(out, "\tmovl $.LC%d, %%edi\n", selectedStr);
    fprintf(out, "\tmovl $0, %%eax\n");
    fprintf(out, "\tcall printf\n");
}

void gencoCall(TAC* tac)
{
    fprintf(out, "\tcall %s\n", tac->arg1->text);
    fprintf(out, "\t.comm %s, 4, 4\n", tac->res->text);
    fprintf(out, "\tmovl %%eax, %s(%%rip)\n", tac->res->text);

    argsPassed = 0;
}

void gencoArg(TAC* tac)
{
    if (tac->res->type == SYMBOL_SCALAR)
        fprintf(out, "\tmovl %s(%%rip), %%%s\n", 
            tac->res->text, argRegisters[argsPassed]);
    else
        fprintf(out, "\tmovl $%ld, %%%s\n", 
            getValue(tac->res->text), argRegisters[argsPassed]);

    argsPassed++;
}

void gencoReturn(TAC* tac)
{
    TAC* aux;
    for (aux = tac; aux->type != TAC_FUNC_END; aux = aux->next);
    fprintf(out, "\tmovl %s(%%rip), %%eax\n", tac->res->text);

    // if (strcmp(aux->res->text, "main"))
    // fprintf(out, "\tpopq %%rbp\n");
    
    // fprintf(out, "\tleave\n");
    // fprintf(out, "\tret\n");
}

void tac2asm(FILE* output, char* filename, TAC* tacList) 
{
    out = output;

    int first = 0;

    for (int i = 0; i < MAX_STRINGS; ++i)
        strings[i] = NULL;
    strCount = 0;

    fprintf(out, "\t.file \"%s\"\n", filename);

    for (TAC* tac = tacList; tac != NULL; tac = tac->next) {
        switch (tac->type) {
            // data
            case TAC_VAR:
                gencoVarDecl(tac);
                break;
            case TAC_ARR:
                gencoArrDecl(tac);
                break;
            case TAC_ARR_DECL:   
                gencoArrInit(tac);
                break;  

            // TODO: SYMBOL?
            case TAC_SYMBOL:
                break;
            
            // labels
            case TAC_LABEL:
                gencoLabel(tac);
                break;

            // arithmetic operators
            case TAC_ADD:
                gencoAdd(tac);
                break;
            case TAC_SUB:
                gencoSub(tac);
                break;
            case TAC_MUL:
                gencoMul(tac);
                break;
            case TAC_DIV:
                gencoDiv(tac);
                break;

            // boolean operators
            case TAC_LT:
            case TAC_GT:
            case TAC_LE:
            case TAC_GE:
                gencoBoolComparison(tac);
                break;
            case TAC_EQ:
            case TAC_NE:
                gencoEqNeq(tac);
                break;
                
            // and, or
            case TAC_AND:
                gencoAnd(tac);
                break;
            case TAC_OR:
                gencoOr(tac);
                break;
            
            // arr access
            case TAC_ARR_READ:
                gencoArrAccess(tac);
                break;

            // mov
            case TAC_MOV:
                gencoMov(tac);
                break;
            case TAC_MOV_IND:
                gencoMovInd(tac);
                break;

            // function start, end
            case TAC_FUNC_START:
                if (first == 0) {
                    fprintf(out, "\t.text\n");
                    first = 1;
                }
                gencoFuncStart(tac);
                break;
            case TAC_FUNC_END:
                gencoFuncEnd(tac);
                break;
            
            // parameters are treated in func_start
            // case TAC_PARAMETER:
        
            // if, jump
            case TAC_IFZ:
                gencoIfz(tac);
                break;
            case TAC_JUMP:
                gencoJump(tac);
                break;
            
            // read, print
            case TAC_READ:
                gencoRead(tac);
                break;
            case TAC_PRINT:
                gencoPrint(tac);
                break;
            
            // calling functions
            case TAC_CALL: 
                gencoCall(tac);
                break;
            // sending arguments to functions
            case TAC_ARG:
                gencoArg(tac);
                break;

            // return
            case TAC_RETURN:     
                gencoReturn(tac);
                break;

            default:
                break;
        }
    }

    fprintf(out, ".LC0:\n");
    fprintf(out, "\t.string \"%%d\\n\"\n");
    for (int i = 0; strings[i] != NULL; ++i) {
        fprintf(out, ".LC%d:\n", i+1);
        fprintf(out, "\t.string %s\n", strings[i]);
    }
}
