/*
 *  Universidade Federal do Rio Grande do Sul
 *  Instituto de Informática
 *  INF01147 - Compiladores (2017/2)
 *
 *  Gabriel Nunes Martins
 *  Leonardo Silva Rosa
 */

%{
    #include <stdio.h>
    #include <stdlib.h>
    #include "hash.h"
    #include "lex.yy.h"

    int getLineNumber();
    int yyerror();
    int yylex();
%}

%token KW_BYTE
%token KW_SHORT
%token KW_LONG
%token KW_FLOAT
%token KW_DOUBLE
%token KW_IF
%token KW_THEN
%token KW_ELSE
%token KW_WHILE
%token KW_FOR
%token KW_READ
%token KW_RETURN
%token KW_PRINT

%token OPERATOR_LE
%token OPERATOR_GE
%token OPERATOR_EQ
%token OPERATOR_NE
%token OPERATOR_AND
%token OPERATOR_OR

%token <symbol>TK_IDENTIFIER
%token <symbol>LIT_INTEGER
%token <symbol>LIT_REAL
%token <symbol>LIT_CHAR
%token <symbol>LIT_STRING

%token TOKEN_ERROR

%union
{
    HASH_NODE *symbol;
}

%nonassoc OPERATOR_AND OPERATOR_OR OPERATOR_EQ OPERATOR_NE '<' OPERATOR_LE '>' OPERATOR_GE
%left '+' '-'
%left '*' '/'

%%

program:              declaration
                    | program declaration;

declaration:          global ';'
                    | function;

global:               variable
                    | array;

type:                 KW_BYTE
                    | KW_SHORT
                    | KW_LONG
                    | KW_FLOAT
                    | KW_DOUBLE;

literal:              LIT_INTEGER 
                    | LIT_REAL
                    | LIT_CHAR
                    // esses três faz sentido mas não tá especificado claramente
                    | '-' LIT_INTEGER
                    | '-' LIT_REAL
                    | '-' LIT_CHAR;

literal_list:         literal
                    | literal literal_list;

variable:             TK_IDENTIFIER ':' type '=' literal;

array:                TK_IDENTIFIER ':' type '[' LIT_INTEGER ']'
                    | TK_IDENTIFIER ':' type '[' LIT_INTEGER ']' literal_list;

function:             '(' type ')' TK_IDENTIFIER '(' ')' block
                    | '(' type ')' TK_IDENTIFIER '(' parameter_list ')' block;

parameter:            TK_IDENTIFIER ':' type;

parameter_list:       parameter
                    | parameter ',' parameter_list;

block:                '{' command_list '}';

command_list:         command
                    | command ';' command_list;

command:              assignment
                    | read
                    | print
                    | return
                    | control_flow
                    | block
                    | ;

assignment:           TK_IDENTIFIER '=' expression
                    | TK_IDENTIFIER '[' expression ']' '=' expression;

read:                 KW_READ '>' TK_IDENTIFIER;

printable:            expression
                    | LIT_STRING;

printable_list:       printable
                    | printable ',' printable_list;

print:                KW_PRINT printable_list;

return:               KW_RETURN expression;

control_flow:         KW_IF '(' expression ')' KW_THEN command
                    | KW_IF '(' expression ')' KW_THEN command KW_ELSE command
                    | KW_WHILE '(' expression ')' command;

expression:           literal
                    | TK_IDENTIFIER
                    | TK_IDENTIFIER '[' expression ']'
                    | '(' expression ')'
                    | expression '+' expression
                    | expression '-' expression
                    | expression '*' expression
                    | expression '/' expression
                    | expression '<' expression
                    | expression '>' expression
                    | expression OPERATOR_LE expression
                    | expression OPERATOR_GE expression
                    | expression OPERATOR_EQ expression
                    | expression OPERATOR_NE expression
                    | expression OPERATOR_AND expression
                    | expression OPERATOR_OR expression
                    //| expression '!' expression
                    | TK_IDENTIFIER '(' ')'
                    | TK_IDENTIFIER '(' expression_list ')'
                    // esses dois faz sentido mas não tá especificado claramente
                    | '!' '(' expression ')'
                    | '-' '(' expression ')';

expression_list:      expression
                    | expression ',' expression_list;


%%

int yyerror() {
    fprintf(stderr, "Syntax error in line %d.\n", getLineNumber());
    exit(3);
}