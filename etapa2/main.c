/*
*  Universidade Federal do Rio Grande do Sul
*  Instituto de Informática
*  INF01147 - Compiladores (2017/2)
*
*  Gabriel Nunes Martins
*  Leonardo Silva Rosa
*/

#include <stdio.h>
#include <stdlib.h>

#include "hash.h"
#include "lex.yy.h"
#include "y.tab.h"

int main(int argv, char *argc[]) {

    if (argv < 2) {
        fprintf(stderr, "Missing arguments.\n");
        exit(1);
    }

    yyin = fopen(argc[1], "r");

    if (!yyin) {
        fprintf(stderr, "Cannot open file - %s\n", argc[1]);
    }

    yyparse();
    fprintf(stderr, "Success!!\n");

    hashPrint();
    exit(0);

}