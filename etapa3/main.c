/*
*  Universidade Federal do Rio Grande do Sul
*  Instituto de Informática
*  INF01147 - Compiladores (2017/2)
*
*  Gabriel Nunes Martins
*  Leonardo Silva Rosa
*/

#include <stdio.h>
#include <stdlib.h>

#include "hash.h"
#include "ast.h"
#include "lex.yy.h"
#include "y.tab.h"

extern AST_NODE *root;

int yyparse();

int main(int argv, char *argc[]) {

    if (argv < 3) {
        fprintf(stderr, "Missing arguments.\n");
        exit(1);
    }

    yyin = fopen(argc[1], "r");

    if (!yyin) {
        fprintf(stderr, "Cannot open file - %s\n", argc[1]);
    }

    yyparse();
    fprintf(stderr, "Success!!\n");


    FILE* output = stdout;
    output = fopen(argc[2], "w");
    
    if (!output) 
    {
        fprintf(stderr, "Cannot open file - %s\n", argc[2]);
        exit(2);
    }
    
    astPrint(root, output);

    fprintf(stderr, "Output success!\n");
    fclose(output);
    exit(0);

}