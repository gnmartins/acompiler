/*
 *  Universidade Federal do Rio Grande do Sul
 *  Instituto de Informática
 *  INF01147 - Compiladores (2017/2)
 *
 *  Gabriel Nunes Martins
 *  Leonardo Silva Rosa
 */

#ifndef AST_H
#define AST_H

#include <stdio.h>
#define MAX_SONS 4

enum AST_TYPE{
	AST_PROGRAM,
	AST_BYTE,
	AST_SHORT,
	AST_LONG,
	AST_FLOAT,
	AST_DOUBLE,
	AST_SYMBOL,
	AST_SYMBOL_NEGATED,
	AST_LITERAL_LIST,
	AST_VAR,
	AST_ARRAY,
	AST_LIT_INT,
	AST_FUNCTION,
	AST_PARAMETER,
	AST_PARAMETER_LIST,
	AST_COMMANDS_BLOCK,
	AST_COMMANDS_LIST,
	AST_COMMAND,
	AST_ASSIGN_VAR,
	AST_ASSIGN_ARRAY,
	AST_READ,
	AST_PRINTABLE,
	AST_PRINT_LIST,
	AST_PRINT,
	AST_RETURN,
	AST_IF,
	AST_IF_ELSE,
	AST_WHILE,
	AST_IDENT,
	AST_IDENT_ARR,
	AST_PARENTHESES,
	AST_ADD,
	AST_SUB,
	AST_MUL,
	AST_DIV,
	AST_LT,
	AST_GT,
	AST_LE,
	AST_GE,
	AST_EQ,
	AST_NE,
	AST_AND,
	AST_OR,
	AST_IDENT_FUN,
	AST_INVERT,
	AST_NEGATE,
	AST_EXPRESSION_LIST
};

typedef struct ast_node
{
	int type;
	HASH_NODE *symbol;
	struct ast_node *sons[MAX_SONS];
} AST_NODE;

AST_NODE *root;
FILE* outputfile;

AST_NODE *astCreate(int type, HASH_NODE *symbol, AST_NODE *son0, AST_NODE *son1, AST_NODE *son2, AST_NODE *son3);
void astPrintNode(AST_NODE *node);
void astPrint(AST_NODE *node, FILE *output);

#endif