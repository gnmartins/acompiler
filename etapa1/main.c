/*
*  Universidade Federal do Rio Grande do Sul
*  Instituto de Informática
*  INF01147 - Compiladores (2017/2)
*  
*  Gabriel Nunes Martins
*  Leonardo Silva Rosa
*/

int main(int argc, char** argv) 
{
    int tok;
    yyin = fopen(argv[1], "r");
    initMe();
    
    while (isRunning()) {
        tok = yylex();
        if (!isRunning()) {
            break;
        }
        
        switch(tok) {
        case KW_BYTE:
            fprintf(stderr, "found KW_BYTE at line %i\n", getLineNumber());
            break;
        case KW_SHORT:
            fprintf(stderr, "found KW_SHORT at line %i\n", getLineNumber());
            break;
        case KW_LONG:
            fprintf(stderr, "found KW_LONG at line %i\n", getLineNumber());
            break;
        case KW_FLOAT:
            fprintf(stderr, "found KW_FLOAT at line %i\n", getLineNumber());
            break;
        case KW_DOUBLE:
            fprintf(stderr, "found KW_DOUBLE at line %i\n", getLineNumber());
            break;
        case KW_IF:
            fprintf(stderr, "found KW_IF at line %i\n", getLineNumber());
            break;
        case KW_THEN:
            fprintf(stderr, "found KW_THEN at line %i\n", getLineNumber());
            break;
        case KW_ELSE:
            fprintf(stderr, "found KW_ELSE at line %i\n", getLineNumber());
            break;
        case KW_WHILE:
            fprintf(stderr, "found KW_WHILE at line %i\n", getLineNumber());
            break;
        case KW_FOR:
            fprintf(stderr, "found KW_FOR at line %i\n", getLineNumber());
            break;
        case KW_READ:
            fprintf(stderr, "found KW_READ at line %i\n", getLineNumber());
            break;
        case KW_RETURN:
            fprintf(stderr, "found KW_RETURN at line %i\n", getLineNumber());
            break;
        case KW_PRINT:
            fprintf(stderr, "found KW_PRINT at line %i\n", getLineNumber());
            break;
        
        case OPERATOR_LE:
            fprintf(stderr, "found OPERATOR_LE at line %i\n", getLineNumber());
            break;
        case OPERATOR_GE:
            fprintf(stderr, "found OPERATOR_GE at line %i\n", getLineNumber());
            break;
        case OPERATOR_EQ:
            fprintf(stderr, "found OPERATOR_EQ at line %i\n", getLineNumber());
            break;
        case OPERATOR_NE:
            fprintf(stderr, "found OPERATOR_NE at line %i\n", getLineNumber());
            break;
        case OPERATOR_AND:
            fprintf(stderr, "found OPERATOR_AND at line %i\n", getLineNumber());
            break;
        case OPERATOR_OR:
            fprintf(stderr, "found OPERATOR_OR at line %i\n", getLineNumber());
            break;
        
        case TK_IDENTIFIER:
            fprintf(stderr, "found TK_IDENTIFIER at line %i\n", getLineNumber());
            break;
        case LIT_INTEGER:
            fprintf(stderr, "found LIT_INTEGER at line %i\n", getLineNumber());
            break;
        case LIT_REAL:
            fprintf(stderr, "found LIT_REAL at line %i\n", getLineNumber());
            break;
        case LIT_CHAR:
            fprintf(stderr, "found LIT_CHAR at line %i\n", getLineNumber());
            break;   
        case LIT_STRING:
            fprintf(stderr, "found LIT_STRING at line %i\n", getLineNumber());
            break;
        
        case TOKEN_ERROR:
            fprintf(stderr, "found TOKEN_ERROR at line %i\n", getLineNumber());
            break;
        
        default:
            fprintf(stderr, "found %c at line %i\n", tok, getLineNumber());
            break;
        }
    }
    fprintf(stderr, "\n------------------ HASH TABLE ------------------\n");
    hashPrint();
    fprintf(stderr, "------------------------------------------------\n");
    return 0;
}