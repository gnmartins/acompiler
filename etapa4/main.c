/*
*  Universidade Federal do Rio Grande do Sul
*  Instituto de Informática
*  INF01147 - Compiladores (2017/2)
*
*  Gabriel Nunes Martins
*  Leonardo Silva Rosa
*/

#include <stdio.h>
#include <stdlib.h>

#include "hash.h"
#include "ast.h"
#include "semantic.h"

#include "lex.yy.h"
#include "y.tab.h"

extern AST_NODE *root;

int yyparse();

int main(int argc, char* argv[]) 
{

    if (argc < 3) {
        fprintf(stderr, "Missing arguments.\n");
        fprintf(stderr, "\t./etapa4 <input.txt> <output.txt>\n");
        exit(1);
    }

    yyin = fopen(argv[1], "r");

    if (!yyin) {
        fprintf(stderr, "Cannot open file - %s\n", argv[1]);
        exit(2);
    }
    
    /* Syntax check */
    yyparse();
    fprintf(stderr, "Syntax check finished successfully!\n");

    /* Semantic check */
    int errCount;
    if ((errCount = semanticCheck(root)) > 0) {
        fprintf(stderr, "Found %d semantic errors.\n", errCount);
        fprintf(stderr, "Semantic check failed!\n");
        exit(4);
    }
    fprintf(stderr, "Semantic check finished successfully!\n");

    FILE* output = stdout;
    output = fopen(argv[2], "w");
    
    if (!output) {
        fprintf(stderr, "Cannot open file - %s\n", argv[2]);
        exit(2);
    }
    
    astPrint(root, output);

    fprintf(stderr, "Output success!\n");
    fclose(output);
    exit(0);

}