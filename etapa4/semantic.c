/*
 *  Universidade Federal do Rio Grande do Sul
 *  Instituto de Informática
 *  INF01147 - Compiladores (2017/2)
 *
 *  Gabriel Nunes Martins
 *  Leonardo Silva Rosa
 */

#include <stdio.h>
#include <stdlib.h>

#include "semantic.h"

int errCount = 0;
FUNC_LIST* func_list = NULL; // declared functions

int semanticCheck(AST_NODE* root) 
{
    declarationCheck(root);
    undefinedSymbolsCheck(root);
    usageCheck(root);
    dataTypeCheck(root);

    return errCount;
}

/* increments error count and prints error message */
void throwError(int lineNumber, char* errMsg) 
{
    errCount++;
    fprintf(stderr, "Semantic error at line %d: %s\n", lineNumber, errMsg);
}

/* checks for duplicate declarations */
void declarationCheck(AST_NODE* node)
{
    if (!node) return;

    if (node->type == AST_VAR 
     || node->type == AST_ARRAY 
     || node->type == AST_FUNCTION
     || node->type == AST_PARAMETER) {
        
        // if symbol is yet to be defined 
        if (node->symbol->type == SYMBOL_UNDEF) {
            setSymbolType(node);
            setSymbolDataType(node);
            node->dataType = node->symbol->dataType;
        } else { // symbol was already defined --- throw error
            char errMsg[256];
            sprintf(errMsg, "Identifier `%s` already declared.", node->symbol->text);
            throwError(node->lineNumber, errMsg);
        }
    }

    // if node is a function, add it to declared functions list
    if (node->type == AST_FUNCTION) {
        addFunction(node);
    }

    for (int i = 0; i < MAX_SONS; ++i) {
        declarationCheck(node->sons[i]);
    }
}

/* checks for calls of undefined symbols */
void undefinedSymbolsCheck(AST_NODE* node)
{
    HASH_NODE* curr;

    // iterating through hash to find undefined symbols
    for (int bucket = 0; bucket < HASH_SIZE; bucket++) {
        for (curr = hashTable[bucket]; curr != NULL; curr = curr->next) {
            if (curr->type == SYMBOL_UNDEF) {
                char errMsg[256];
                sprintf(errMsg, "Identifier `%s` is undefined.", curr->text);
                throwError(curr->lineNumber, errMsg);
            }
        }
    }
}

/* checks if identifiers are being used correctly */
void usageCheck(AST_NODE* node) 
{
    if (!node) return;

    switch (node->type) {
        // TODO: discover when this is called...
        case AST_VAR:
            if (node->symbol->type != SYMBOL_SCALAR) {
                throwError(node->lineNumber, "Iss usage.");
            }
            break;
        
        // arr = bla --- arr should be indexed
        case AST_ASSIGN_VAR:
            if (node->symbol->type != SYMBOL_SCALAR) {
                char errMsg[256];
                sprintf(errMsg, "Expected a variable, but found `%s`.", node->symbol->text);
                throwError(node->lineNumber, errMsg);
            }
            break;
        
        // var[0] = bla OR bla = var[0] --- var can't be indexed
        case AST_ASSIGN_ARRAY:
        case AST_IDENT_ARR:
            if (node->symbol->type != SYMBOL_ARRAY) {
                char errMsg[256];
                sprintf(errMsg, "Expected an array, but found `%s`.", node->symbol->text);
                throwError(node->lineNumber, errMsg);
            }
            break;
        
        // trying to call variable as a function
        case AST_IDENT_FUN:
            if (node->symbol->type != SYMBOL_FUNCTION) {
                char errMsg[256];
                sprintf(errMsg, "Expected a function, but found `%s`.", node->symbol->text);
                throwError(node->lineNumber, errMsg);
            }
            break;
        
        default:
            break;
    }

    for (int i = 0; i < MAX_SONS; ++i) {
        usageCheck(node->sons[i]);
    }
}

void dataTypeCheck(AST_NODE* node)
{
    if (!node) return;

    for (int i = 0; i < MAX_SONS; ++i) {
        dataTypeCheck(node->sons[i]);
    }

    switch (node->type) {
        // declaration types
        case AST_BYTE:
        case AST_SHORT:
        case AST_LONG:
            node->dataType = DATATYPE_INTEGER;
            break;
        case AST_FLOAT:
        case AST_DOUBLE:
            node->dataType = DATATYPE_REAL;
            break;

        // identifiers and symbols
        case AST_IDENT:
        case AST_IDENT_ARR:
        case AST_IDENT_FUN:
        case AST_SYMBOL:
        case AST_SYMBOL_NEGATED:
            node->dataType = node->symbol->dataType;
            
            // checking arguments type
            if (node->type == AST_IDENT_FUN) {
                funcArgumentsCheck(node);
            }

            break;
        
        // list of literals (array initialization)
        case AST_LITERAL_LIST:
            // if the type of the remainder of the list is not the same as this positions,
            // then the datatype of the list will be undefined
            node->dataType = node->sons[0]->dataType;           
            if (node->sons[1])
                if (node->dataType != node->sons[1]->dataType) 
                    node->dataType = DATATYPE_UNDEF;
            break;

        // variable initialization must have compatible types
        case AST_VAR:
            // both declared type (0) and assigned type (1) must get the same result
            // in integer check (i.e. both are integers or both are reals)
            if (integerTypeCheck(node->sons[0]->dataType) != 
                integerTypeCheck(node->sons[1]->dataType)) 
                    throwError(node->lineNumber, "Incompatible types.");
            break;

        // array initialization must have compatible types
        case AST_ARRAY:
            // if (2) is not null, then the array is being initialized in declaration
            if (node->sons[2] != NULL) {
                // both declared type (0) and assigned type (2) must get the same result
                // in integer check (i.e. both are integers or both are reals)
                // *** assigned type must not be inconsistent (DATATYPE_UNDEF)
                if (integerTypeCheck(node->sons[0]->dataType) != 
                    integerTypeCheck(node->sons[2]->dataType) ||
                    node->sons[2]->dataType == DATATYPE_UNDEF)
                        throwError(node->lineNumber, "Incompatible types.");
            }
            break;

        // operators must not be boolean
        case AST_LT:
        case AST_GT:
        case AST_LE:
        case AST_GE:
            if (node->sons[0]->dataType == DATATYPE_BOOL ||
                node->sons[1]->dataType == DATATYPE_BOOL)
                throwError(node->lineNumber, "Wrong operator type (unexpected boolean operator).");

            node->dataType = DATATYPE_BOOL;            
            break;
        
        // operators must have equivalent type
        case AST_EQ:
        case AST_NE:
            if ((node->sons[0]->dataType == DATATYPE_BOOL && node->sons[1]->dataType != DATATYPE_BOOL) ||
                (node->sons[0]->dataType != DATATYPE_BOOL && node->sons[1]->dataType == DATATYPE_BOOL))
                throwError(node->lineNumber, "Wrong operator type (expected operators with same type).");
            
            node->dataType = DATATYPE_BOOL;            
            break;
        
        // operators must be boolean
        case AST_AND:
        case AST_OR:
            if (node->sons[0]->dataType != DATATYPE_BOOL ||
                node->sons[1]->dataType != DATATYPE_BOOL)
                throwError(node->lineNumber, "Wrong operator type (expected boolean).");
            
            node->dataType = DATATYPE_BOOL;
            break;
        
        // arithmetic operators have the "wider" type
        case AST_ADD:
        case AST_SUB:
        case AST_MUL:
        case AST_DIV:
            if (node->sons[0]->dataType == DATATYPE_BOOL ||
                node->sons[1]->dataType == DATATYPE_BOOL)
                throwError(node->lineNumber, "Wrong operator type (unexpected boolean operator).");
            
            if (node->type != AST_DIV) {
                node->dataType = getArithmeticType(node->sons[0]->dataType, 
                                                   node->sons[1]->dataType);
            } else {
                // TODO: divisão gera sempre float/double ou qualquer coisa?
                node->dataType = DATATYPE_DOUBLE;
            }
            break;

        // parentheses have the same type of their inside
        case AST_PARENTHESES:
            node->dataType = node->sons[0]->dataType;
			break;
        
        // LHS and RHS of assignment must have compatible types
        case AST_ASSIGN_VAR:
            if (!typeCompatibilityCheck(node->symbol->dataType, node->sons[0]->dataType)) {
                throwError(node->lineNumber, "Incompatible types.");
            }
            if (node->sons[0]->type == AST_IDENT)
                if (node->sons[0]->symbol->type == SYMBOL_FUNCTION || node->sons[0]->symbol->type == SYMBOL_ARRAY)
                    throwError(node->lineNumber, "Missing function/array complement.");

            if (integerTypeCheck(node->symbol->dataType) != integerTypeCheck(node->sons[0]->dataType))
                throwError(node->lineNumber, "Incompatible types.");
            break;
        
        // LHS and RHS of assignment must have compatible types
        // index must be integer
        case AST_ASSIGN_ARRAY:
            if (!integerTypeCheck(node->sons[0]->dataType)) {
                throwError(node->lineNumber, "Array index must be an integer.");
            }
            if (!typeCompatibilityCheck(node->symbol->dataType, node->sons[1]->dataType)) {
                throwError(node->lineNumber, "Incompatible types.");
            }
            if (node->sons[1]->type == AST_IDENT)
                if (node->sons[1]->symbol->type == SYMBOL_FUNCTION || node->sons[1]->symbol->type == SYMBOL_ARRAY)
                    throwError(node->lineNumber, "Missing function/array complement.");

            if (integerTypeCheck(node->symbol->dataType) != integerTypeCheck(node->sons[1]->dataType))
                throwError(node->lineNumber, "Incompatible types.");
            break;
        
        // expression must be boolean
        case AST_INVERT:
            if (node->sons[0]->dataType != DATATYPE_BOOL)
                throwError(node->lineNumber, "Wrong operator type (expected boolean).");
            break;

        // expression must no be boolean
        case AST_NEGATE:
            if (node->sons[0]->dataType == DATATYPE_BOOL)
                throwError(node->lineNumber, "Wrong operator type (expected boolean).");
            
            break;

        // conditional expression must be boolean
        case AST_IF:
        case AST_IF_ELSE:
        case AST_WHILE:
            if (node->sons[0]->dataType != DATATYPE_BOOL)
                throwError(node->sons[0]->lineNumber, "Conditional expression must be boolean.");
            break;
            
        case AST_RETURN:
            if (node->sons[0]->dataType == DATATYPE_BOOL)
                throwError(node->sons[0]->lineNumber, "Invalid return type (must not be boolean).");
            break; 
        /*
            //AST_LITERAL_LIST,
            //AST_VAR,
            //AST_ARRAY,
            //AST_LIT_INT,
            //AST_FUNCTION,
            //AST_PARAMETER,
            //AST_PARAMETER_LIST,
            //AST_COMMANDS_BLOCK,
            //AST_COMMANDS_LIST,
            //AST_COMMAND,

            //AST_READ,
            //AST_PRINTABLE,
            //AST_PRINT_LIST,
            //AST_PRINT,
            //AST_RETURN,

            //AST_EXPRESSION_LIST
        */
        default:
            break;
    }
}

/* sets hash symbol type based on ast node type */
void setSymbolType(AST_NODE* node)
{
    switch (node->type) {
        case AST_VAR:
            node->symbol->type = SYMBOL_SCALAR;
            break;

        case AST_ARRAY:
            node->symbol->type = SYMBOL_ARRAY;
            break;

        case AST_FUNCTION:
            node->symbol->type = SYMBOL_FUNCTION;
            break;
        
        case AST_PARAMETER:
            node->symbol->type = SYMBOL_SCALAR;
            break;
        
        default:
            node->symbol->type = SYMBOL_UNDEF;
            break;
    }
}

/* sets hash symbol datatype based on ast node type */
void setSymbolDataType(AST_NODE* node)
{
    switch (node->sons[0]->type) {
        case AST_BYTE:
            node->symbol->dataType = DATATYPE_BYTE;
            break;

	    case AST_SHORT:
            node->symbol->dataType = DATATYPE_SHORT;
            break;

	    case AST_LONG:
            node->symbol->dataType = DATATYPE_LONG;
            break;

	    case AST_FLOAT:
            node->symbol->dataType = DATATYPE_FLOAT;
            break;

	    case AST_DOUBLE:
            node->symbol->dataType = DATATYPE_DOUBLE;
            break;
        
        case AST_LIT_INT:
            node->symbol->dataType = DATATYPE_INTEGER;
            break;

        default:
            node->symbol->dataType = SYMBOL_UNDEF;
            break;
    }
}

/* adds declared function in ast node to the list */
void addFunction(AST_NODE* node)
{
    FUNC_LIST* newFunc;

	if (!(newFunc = (FUNC_LIST*) calloc(1, sizeof(FUNC_LIST)))) {
		fprintf(stderr, "Error while adding function declaration: memmory allocation failed.\n");
		exit(1);
	}

    newFunc->next = func_list;
    newFunc->node = node;

    func_list = newFunc;

    //printFunctions();
}

/* iterates through function list and print its identifiers */
void printFunctions() 
{
    fprintf(stderr, "-- declared functions --\n");
    for (FUNC_LIST* f = func_list; f != NULL; f = f->next) {
        fprintf(stderr, "function `%s`\n", f->node->symbol->text);
    }
    fprintf(stderr, "------------------------\n");    
}

/* returns wider type between operators */
int getArithmeticType(int type1, int type2)
{
    if (type1 == DATATYPE_BOOL || type2 == DATATYPE_BOOL)
        return DATATYPE_UNDEF;
    
    // this makes sense since the datatypes are declared 
    // in a ascending order of "wideness" (i.e. datatypes declared
    // after in hash.h are bigger)
    if (type1 > type2) {
        return type1;
    } else {
        return type2;
    }
}

/* returns 1 if types are compatible */
int typeCompatibilityCheck(int type1, int type2) 
{
    // this is covered in the next ifs --- added for clarity
    //if (type1 == DATATYPE_UNDEF || type2 == DATATYPE_UNDEF)
    //    return 0;

    // compatibility between integers and floating point types
    if ((type1 >= DATATYPE_INTEGER && type1 <= DATATYPE_DOUBLE) ||
        (type2 >= DATATYPE_INTEGER && type2 <= DATATYPE_DOUBLE)) {
        return 1;
    }

    // compatibility between bools, strings and chars
    //if (type1 == type2) return 1;

    // TODO: compatibility between char and string?

    return 0;
}

/* returns 1 if type is an integer */
int integerTypeCheck(int type)
{
    if (type >= DATATYPE_INTEGER && type <= DATATYPE_LONG) {
        return 1;
    } else {
        return 0;
    }
}

/* returns 1 if arguments types match the function definition */
int funcArgumentsCheck(AST_NODE* funcCall)
{
    AST_NODE* funcDeclaration;
    if (!(funcDeclaration = findFuncDeclaration(funcCall))) {
        char errMsg[256];
        sprintf(errMsg, "Function `%s` was not declared.", funcCall->symbol->text);
        throwError(funcCall->lineNumber, errMsg);
        return 0;
    }

    AST_NODE* parameters = funcDeclaration->sons[1];
    AST_NODE* arguments = funcCall->sons[0];

    while (parameters && arguments) {
        if (!typeCompatibilityCheck(parameters->sons[0]->dataType, arguments->sons[0]->dataType)) {
            throwError(funcCall->lineNumber, "Argument type mismatch.");
            return 0;
        }
        if (integerTypeCheck(parameters->sons[0]->dataType) != integerTypeCheck(arguments->sons[0]->dataType)) {
            throwError(funcCall->lineNumber, "Argument type mismatch.");
            return 0;
        }
        parameters = parameters->sons[1];
        arguments = arguments->sons[1];
    }

    if (parameters) {
        throwError(funcCall->lineNumber, "Missing arguments for function call.");
    } else if (arguments) {
        throwError(funcCall->lineNumber, "Too many arguments on function call.");
    }

    return 1;
}

/* returns the function declaration node based on the function call node */
AST_NODE* findFuncDeclaration(AST_NODE* funcCall)
{
    for (FUNC_LIST* curr = func_list; curr != NULL; curr = curr->next) {
        if (curr->node->symbol->text == funcCall->symbol->text)
            return curr->node;
    }
    return NULL;
}
