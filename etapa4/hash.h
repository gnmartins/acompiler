/*
 *  Universidade Federal do Rio Grande do Sul
 *  Instituto de Informática
 *  INF01147 - Compiladores (2017/2)
 *
 *  Gabriel Nunes Martins
 *  Leonardo Silva Rosa
 */

#ifndef HASH_H
#define HASH_H

#define SYMBOL_IDENTIFIER   1
#define SYMBOL_LIT_INTEGER  2
#define SYMBOL_LIT_REAL     3
#define SYMBOL_LIT_CHAR     4
#define SYMBOL_LIT_STRING   5

enum SYMBOL {
    SYMBOL_UNDEF,
    SYMBOL_LITERAL,
    SYMBOL_SCALAR,
    SYMBOL_ARRAY, 
    SYMBOL_FUNCTION
};

enum DATATYPE {
    DATATYPE_UNDEF,     //0
    DATATYPE_STRING,    //1

    DATATYPE_INTEGER,   //2
    DATATYPE_CHAR,      //3
    DATATYPE_BYTE,      //4
	DATATYPE_SHORT,     //5
    DATATYPE_LONG,      //6
    
    DATATYPE_REAL,      //7
	DATATYPE_FLOAT,     //8
	DATATYPE_DOUBLE,    //9

    DATATYPE_BOOL,      //10
};

#define HASH_SIZE 1223

typedef struct hash_node {
    int type;
    char* text;
    struct hash_node* next;

    int dataType;
    int lineNumber;
} HASH_NODE;

HASH_NODE* hashTable[HASH_SIZE];

void hashInit(void);
int hashAddress(char* text);
HASH_NODE* hashInsert(int type, char* text, int dataType, int lineNumber);
HASH_NODE* hashFind(char* text);
void hashPrint(void);

#endif
