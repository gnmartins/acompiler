/*
 *  Universidade Federal do Rio Grande do Sul
 *  Instituto de Informática
 *  INF01147 - Compiladores (2017/2)
 *
 *  Gabriel Nunes Martins
 *  Leonardo Silva Rosa
 */

#ifndef TAC_H
#define TAC_H

#include "hash.h"
#include "ast.h"

typedef struct tac_node {
    int type;

    HASH_NODE* res;
    HASH_NODE* arg1;
    HASH_NODE* arg2;

    struct tac_node* prev;
    struct tac_node* next;
} TAC;

enum TAC_TYPE {
    TAC_SYMBOL,
    TAC_TEMP,
    TAC_LABEL,

    TAC_ADD,
	TAC_SUB,
	TAC_MUL,
	TAC_DIV,
	TAC_LT,
	TAC_GT,
	TAC_LE,
	TAC_GE,
	TAC_EQ,
	TAC_NE,
	TAC_AND,
	TAC_OR,

    TAC_ARR_READ,

    TAC_MOV,
    TAC_MOV_IND,

    TAC_FUNC_START,
    TAC_FUNC_END,
    TAC_PARAMETER,

    TAC_IFZ,
    TAC_JUMP,

    TAC_READ,
    TAC_PRINT,
    TAC_CALL, 
    TAC_ARG,

    TAC_RETURN
};

TAC* tacCreate(int type, HASH_NODE* res, HASH_NODE* arg1, HASH_NODE* arg2);
TAC* tacReverseList(TAC *list);
void tacPrint(TAC* tac);
void tacPrintList(TAC* list);
TAC* tacJoin(TAC* tac1, TAC* tac2);
char* tacGetTypeAsString(int type);
/* ------------------------------------------------------------------------------------ */
TAC* tacGenerate(AST_NODE* node);

TAC* tacMakeBinOp(int type, TAC* op1, TAC* op2);

TAC* tacMakeArrAccess(HASH_NODE* symbol, TAC* code[]);

TAC* tacMakeVarAssignment(HASH_NODE* symbol, TAC* code[]);
TAC* tacMakeArrAssignment(HASH_NODE* symbol, TAC* code[]);

TAC* tacMakeFuncDefinition(HASH_NODE* symbol, TAC* code[]);
TAC* tacMakeParameter(HASH_NODE* symbol, TAC* code[]);

TAC* tacMakeIf(HASH_NODE* symbol, TAC* code[]);
TAC* tacMakeIfElse(HASH_NODE* symbol, TAC* code[]);
TAC* tacMakeWhile(HASH_NODE* symbol, TAC* code[]);

TAC* tacMakeRead(HASH_NODE* symbol, TAC* code[]);
TAC* tacMakePrint(HASH_NODE* symbol, TAC* code[]);
TAC* tacMakeFuncCall(HASH_NODE* symbol, TAC* code[]);
TAC* tacMakeArgument(HASH_NODE* symbol, TAC* code[]);
TAC* tacMakePrintable(HASH_NODE* symbol, TAC* code[]);

TAC* tacMakeReturn(HASH_NODE* symbol, TAC* code[]);

#endif