/*
 *  Universidade Federal do Rio Grande do Sul
 *  Instituto de Informática
 *  INF01147 - Compiladores (2017/2)
 *
 *  Gabriel Nunes Martins
 *  Leonardo Silva Rosa
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "hash.h"

void hashInit(void)
{
    int i;
    for (i = 0; i < HASH_SIZE; ++i) {
        hashTable[i] = NULL;
    }
}

int hashAddress(char* text)
{
    int i = 0;
    int address = 1;
    for (i = 0; i < strlen(text); ++i) {
        address = (address * text[i]) % HASH_SIZE + 1;
    }
    return address - 1;
}

HASH_NODE* hashInsert(int type, char* text, int dataType, int lineNumber)
{
    int address;
    HASH_NODE* newNode = NULL;

    address = hashAddress(text);

    newNode = hashFind(text);
    if (newNode == NULL) {
        newNode = (HASH_NODE*) calloc(1, sizeof(HASH_NODE));
        newNode->type = type;
        newNode->text = (char*) calloc(strlen(text)+1, sizeof(char));
        strcpy(newNode->text, text);

        newNode->next = hashTable[address];
        hashTable[address] = newNode;

        newNode->dataType = dataType;
        newNode->lineNumber = lineNumber;
    }
    return newNode;
}

HASH_NODE* hashFind(char* text)
{
    int address;
    address = hashAddress(text);

    HASH_NODE* node;

    for (node = hashTable[address]; node != NULL; node = node->next) {
        if (!strcmp(text, node->text))
            return node;
    }

    return NULL;
}

void hashPrint(void)
{
    int i = 0;
    HASH_NODE* node;

    for (i = 0; i < HASH_SIZE; ++i) {
        for (node = hashTable[i]; node != NULL; node = node->next) {
            fprintf(stderr, "Bucket %d has %s of type %d (datatype %d, line %d)\n",
                    i, node->text, node->type, node->dataType, node->lineNumber);
        }
    }
}

HASH_NODE* hashCreateTemp()
{
    static int tempCount = 0;
    char buffer[256];

    sprintf(buffer, "__temp%d", tempCount);
    tempCount++;

    return hashInsert(SYMBOL_SCALAR, buffer, DATATYPE_UNDEF, 0);
}

HASH_NODE* hashCreateLabel()
{
    static int labelCount = 0;
    static char buffer[256];

    sprintf(buffer, "__label%d", labelCount);
    labelCount++;

    return hashInsert(SYMBOL_LABEL, buffer, DATATYPE_UNDEF, 0);
}

HASH_NODE* hashCreateLabelArrayInit(HASH_NODE* node)
{
    static char buffer[256];

    sprintf(buffer, "__init%s", node->text);
    
    return hashInsert(SYMBOL_LABEL, buffer, DATATYPE_UNDEF, 0);
}


