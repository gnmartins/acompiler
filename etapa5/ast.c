/*
 *  Universidade Federal do Rio Grande do Sul
 *  Instituto de Informática
 *  INF01147 - Compiladores (2017/2)
 *
 *  Gabriel Nunes Martins
 *  Leonardo Silva Rosa
 */

#include <stdio.h>
#include <stdlib.h>
#include "hash.h"
#include "ast.h"

AST_NODE* astCreate(int type, int lineNumber, HASH_NODE* symbol, 
	AST_NODE* son0, AST_NODE* son1, AST_NODE* son2, AST_NODE* son3)
{
	AST_NODE* newNode;

	if (!(newNode = (AST_NODE*) calloc(1, sizeof(AST_NODE)))) {
		fprintf(stderr, "Error while creating AST: memmory allocation failed.\n");
		exit(1);
	}

	newNode->type = type;
	newNode->symbol = symbol;
	newNode->sons[0] = son0;
	newNode->sons[1] = son1;
	newNode->sons[2] = son2;
	newNode->sons[3] = son3;

	newNode->lineNumber = lineNumber;
	newNode->dataType = DATATYPE_UNDEF;

	return newNode;
}

void astPrintNode(AST_NODE* node) 
{
	astPrint(node, stderr);
}

void astPrint(AST_NODE* node, FILE* output)
{
	if (node) {
		switch (node->type)	{
			/* --- program --- */
			case AST_PROGRAM:
				astPrint(node->sons[0], output);
				if (node->sons[1]) {
					astPrint(node->sons[1], output);
				}
				break;
			/* ---------------------------- */

			/* --- types --- */
			case AST_FLOAT:
				fprintf(output, "float");
				break;

			case AST_BYTE:
				fprintf(output, "byte");
				break;

			case AST_SHORT:
				fprintf(output, "short");
				break;

			case AST_LONG:
				fprintf(output, "long");
				break;

			case AST_DOUBLE:
				fprintf(output, "double");
				break;

			case AST_LIT_INT: 
				if(node->symbol)
					fprintf(output, "%s", node->symbol->text); 
				break;
			/* ---------------------------- */

			/* --- symbol --- */
			case AST_SYMBOL:
				fprintf(output, "%s", node->symbol->text);
				break;

			case AST_SYMBOL_NEGATED:
				fprintf(output, "- %s", node->symbol->text);
				break;
			/* ---------------------------- */

			/* --- literals --- */
			case AST_LITERAL_LIST: 
				astPrint (node->sons[0], output);
				fprintf(output, " ");

				if (node->sons[1])
					astPrint (node->sons[1], output);
				break;
			/* ---------------------------- */

			/* --- globals --- */
			case AST_VAR: // myVar: double 40;
				if(node->symbol)
					fprintf(output, "%s: ", node->symbol->text);

				astPrint (node->sons[0], output);
				fprintf(output, " = ");
				astPrint (node->sons[1], output);
				fprintf(output, ";\n");
				break;
			
			case AST_ARRAY:  // myArray: long[5]
				if(node->symbol)
					fprintf(output, "%s: ", node->symbol->text);
				
				astPrint (node->sons[0], output);
				fprintf(output, "[");
				astPrint (node->sons[1], output);
				fprintf(output, "]");

				if (node->sons[2]) {
					fprintf(output, " ");
					astPrint (node->sons[2], output);
				}

				fprintf(output, ";\n");
				break;
			/* ---------------------------- */

			/* --- functions --- */
			case AST_FUNCTION:
				// (type)
				fprintf(output, "(");
				astPrint (node->sons[0], output);
				fprintf(output, ") ");

				// identifier
				if(node->symbol)
					fprintf(output, "%s", node->symbol->text);

				// (parameters)
				fprintf(output, "(");
				if(node->sons[1])
					astPrint (node->sons[1], output);
				fprintf(output, ")\n");

				// block
				astPrint (node->sons[2], output);

				fprintf(output, "\n");
				break;

			case AST_PARAMETER: 
				fprintf(output, "%s : ", node->symbol->text);
				astPrint (node->sons[0], output);

				break;

			case AST_PARAMETER_LIST: 
				astPrint (node->sons[0], output);
				if (node->sons[1]) {
					fprintf(output, ", ");
					astPrint (node->sons[1], output);
				}

				break;
			/* ---------------------------- */

			/* --- commands --- */
			case AST_COMMANDS_BLOCK:
				fprintf(output, "{\n");
				astPrint (node->sons[0], output);
				fprintf(output, "\n}");
				break;
			
			case AST_COMMANDS_LIST:
				astPrint (node->sons[0], output);
				if (node->sons[1]) {
					fprintf (output, ";\n");
					astPrint (node->sons[1], output);
				}
				break;

			case AST_COMMAND:
				astPrint (node->sons[0], output);
				break;
			/* ---------------------------- */

			/* --- attribution --- */
			case AST_ASSIGN_VAR:
				fprintf(output, "%s = ", node->symbol->text);
				astPrint(node->sons[0], output);
				break;

			case AST_ASSIGN_ARRAY:
				fprintf(output, "%s[", node->symbol->text);
				astPrint (node->sons[0], output);
				fprintf (output, "] = ");
				astPrint (node->sons[1], output);
				break;
			/* ---------------------------- */

			/* --- print, return, read etc --- */
			case AST_READ:
				fprintf (output, "read > ");
				if(node->symbol)
					fprintf(output, "%s", node->symbol->text);

				break;

			case AST_PRINTABLE:
				if (node->symbol)
					fprintf(output, "%s", node->symbol->text);
				else
					astPrint (node->sons[0], output);

				break;

			case AST_PRINT_LIST:
				astPrint(node->sons[0], output);
				if(node->sons[1]) {
					fprintf(output, ", ");
					astPrint(node->sons[1], output);
				}

				break;

			case AST_PRINT:
				fprintf (output, "print ");
				astPrint (node->sons[0], output);
				break;

			case AST_RETURN:
				fprintf (output, "return ");
				astPrint (node->sons[0], output);
				break;
			/* ---------------------------- */

			/* --- control flow --- */
			case AST_IF:
				fprintf(output, "if (");
				astPrint (node->sons[0], output);
				fprintf(output, ") then\n");
				astPrint (node->sons[1], output);
				break;

			case AST_IF_ELSE:
				fprintf(output, "if (");
				astPrint (node->sons[0], output);
				fprintf(output, ") then\n");
				astPrint (node->sons[1], output);
				fprintf(output, "\nelse\n");
				astPrint (node->sons[2], output);
				break;

			case AST_WHILE:
	  			fprintf(output, "while (");
				astPrint (node->sons[0], output);
				fprintf(output, ")\n");
				astPrint (node->sons[1], output); 
				break;
			/* ---------------------------- */

			/* --- expressions --- */
			case AST_IDENT:
				if(node->symbol)
					fprintf(output, "%s", node->symbol->text);

				break;

			case AST_IDENT_ARR:  // TK_IDENTIFIER '[' expression ']'
				if(node->symbol)
					fprintf(output, "%s", node->symbol->text);

				fprintf(output, "[");
				astPrint (node->sons[0], output);
				fprintf(output, "]");
				break;

			case AST_IDENT_FUN:  // TK_IDENTIFIER '(' ')' | TK_IDENTIFIER '(' expression_list ')'
				if(node->symbol)
					fprintf (output, "%s", node->symbol->text);

				fprintf (output, "(");
			
				if(node->sons[0])
					astPrint (node->sons[0], output);

				fprintf (output, ")");
				break;

			case AST_EXPRESSION_LIST:
				astPrint(node->sons[0], output);
				if(node->sons[1]) {
					fprintf(output, ", ");
					astPrint(node->sons[1], output);
				}

				break;

			case AST_PARENTHESES:  // '(' expression ')' 
				fprintf (output, "(");
				astPrint (node->sons[0], output);
				fprintf (output, ")");
				break;
			/* ---------------------------- */

			/* --- arithmetic operators --- */
			case AST_ADD:
				astPrint(node->sons[0], output);
				fprintf(output, " + ");
				astPrint(node->sons[1], output);
				break;
			
			case AST_SUB:
				astPrint(node->sons[0], output);
				fprintf(output, " - ");
				astPrint(node->sons[1], output);
				break;
			
			case AST_MUL:
				astPrint(node->sons[0], output);
				fprintf(output, " * ");
				astPrint(node->sons[1], output);
				break;
			
			case AST_DIV:
				astPrint(node->sons[0], output);
				fprintf(output, " / ");
				astPrint(node->sons[1], output);
				break;

			case AST_NEGATE:
				fprintf(output, "-(");
				astPrint(node->sons[0], output);
				fprintf(output, ")");
				break;
			/* ---------------------------- */

    		/* --- logical operators --- */
			case AST_OR:
				astPrint(node->sons[0], output);
				fprintf(output, " || ");
				astPrint(node->sons[1], output);
				break;

	  		case AST_AND:
    			astPrint(node->sons[0], output);
				fprintf(output, " && ");
				astPrint(node->sons[1], output);
				break;
			
			case AST_NE:
		    	astPrint(node->sons[0], output);
				fprintf(output, " != ");
				astPrint(node->sons[1], output);
				break;

  			case AST_EQ:
	    		astPrint(node->sons[0], output);
				fprintf(output, " == ");
				astPrint(node->sons[1], output);
				break;

			case AST_GE:
				astPrint(node->sons[0], output);
				fprintf(output, " >= ");
				astPrint(node->sons[1], output);
				break;

			case AST_LE:
				astPrint(node->sons[0], output);
				fprintf(output, " <= ");
				astPrint(node->sons[1], output);
				break;

			case AST_LT:
				astPrint(node->sons[0], output);
				fprintf(output, " < ");
				astPrint(node->sons[1], output);
				break;

			case AST_GT:
				astPrint(node->sons[0], output);
				fprintf(output, " > ");
				astPrint(node->sons[1], output);
				break;

  			case AST_INVERT:
				fprintf(output, "!(");
				astPrint(node->sons[0], output);
				fprintf(output, ")");
				break;
			/* ---------------------------- */

			default: 
				fprintf(output, "No match!");
				break;
		}
	}
}



