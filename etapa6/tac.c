/*
 *  Universidade Federal do Rio Grande do Sul
 *  Instituto de Informática
 *  INF01147 - Compiladores (2017/2)
 *
 *  Gabriel Nunes Martins
 *  Leonardo Silva Rosa
 */

#include <stdio.h>
#include <stdlib.h>
#include "tac.h"

TAC* tacCreate(int type, HASH_NODE* res, HASH_NODE* arg1, HASH_NODE* arg2)
{
    TAC* tac;

	if (!(tac = (TAC*) calloc(1, sizeof(TAC)))) {
		fprintf(stderr, "Error while creating TAC: memmory allocation failed.\n");
		exit(1);
	}

    tac->type = type;
    tac->res = res;
    tac->arg1 = arg1;
    tac->arg2 = arg2;

    tac->prev = NULL;
    tac->next = NULL;

    return tac;
}

void tacPrint(TAC* tac)
{
    if (!tac) return;
    /*
    if (tac->type == TAC_SYMBOL ||
        tac->type == TAC_VAR ||
        tac->type == TAC_ARR ||
        tac->type == TAC_ARR_DECL) return;
    */
    fprintf(stderr, "(%s) %s | %s | %s\n",
        tacGetTypeAsString(tac->type),
        tac->res? tac->res->text : "N/A",
        tac->arg1? tac->arg1->text : "N/A",
        tac->arg2? tac->arg2->text : "N/A");
}

void tacPrintList(TAC* list)
{
    for (TAC* aux = list; aux != NULL; aux = aux->next)
        tacPrint(aux);
}

/* joins two list of TACs */
TAC* tacJoin(TAC* tac1, TAC* tac2)
{
    if (!tac1) return tac2;
    if (!tac2) return tac1;

    TAC* aux;
    for (aux = tac2; aux->prev != NULL; aux = aux->prev) {}

    aux->prev = tac1;
    tac1->next = aux;
    return tac2;
}

/* returns the first element of the TAC list */
TAC* tacReverseList(TAC *list) 
{
    TAC* aux = list;

    if (!list) return NULL;

    while (aux->prev) 
        aux = aux->prev;
    
    return aux;
}

/* returns a string based on type */
char* tacGetTypeAsString(int type)
{
    switch (type) {  
        case TAC_VAR:
            return "       VAR";
        case TAC_ARR:
            return "       ARR";
        case TAC_ARR_DECL:
            return "  ARR_DECL";
        case TAC_LABEL:
            return "     LABEL";

        case TAC_ADD:
            return "       ADD";
        case TAC_SUB:
            return "       SUB";
        case TAC_MUL:
            return "       MUL";
        case TAC_DIV:
            return "       DIV";
        case TAC_LT:
            return "        LT";
        case TAC_GT:
            return "        GT";
        case TAC_LE:
            return "        LE";
        case TAC_GE:
            return "        GE";
        case TAC_EQ:
            return "        EQ";
        case TAC_NE:
            return "        NE";
        case TAC_AND:
            return "       AND";
        case TAC_OR:
            return "        OR";

        case TAC_ARR_READ:
            return "  ARR_READ";

        case TAC_MOV:
            return "       MOV";
        case TAC_MOV_IND:
            return "   MOV_IND";

        case TAC_FUNC_START:
            return "FUNC_START";
        case TAC_FUNC_END:
            return "  FUNC_END";
        case TAC_PARAMETER:
            return "     PARAM";

        case TAC_IFZ:
            return "       IFZ";
        case TAC_JUMP:
            return "      JUMP";

        case TAC_READ:
            return "      READ";
        case TAC_PRINT:
            return "     PRINT";
        case TAC_CALL:
            return "      CALL";
        case TAC_ARG:
            return "       ARG";

        case TAC_RETURN:
            return "    RETURN";

        default:
            return "     UNDEF";
    }
}

/* returns a TAC (or list of TACs) based on the AST node */
TAC* tacGenerate(AST_NODE* node)
{
    if (!node) return NULL;

    TAC* code[MAX_SONS];

    for (int i = 0; i < MAX_SONS; ++i) {
        code[i] = tacGenerate(node->sons[i]);
    }

    switch (node->type) {
        
        case AST_VAR:
            return tacCreate(TAC_VAR, node->symbol,
                code[1]? code[1]->res : NULL, NULL);
        case AST_ARRAY:
            return tacJoin(tacCreate(TAC_ARR, node->symbol,
                code[1]? code[1]->res : NULL, NULL), code[2]);
        case AST_LITERAL_LIST:
            return tacJoin(tacCreate(TAC_ARR_DECL, code[0]? code[0]->res : NULL, NULL, NULL),
                code[1]);
        
        // symbols
        case AST_LIT_INT:
        case AST_SYMBOL:
        case AST_IDENT:
            return tacCreate(TAC_SYMBOL, node->symbol, NULL, NULL);
            
        // binary operations
        case AST_ADD:
            return tacMakeBinOp(TAC_ADD, code[0], code[1]);
	    case AST_SUB:
            return tacMakeBinOp(TAC_SUB, code[0], code[1]);
	    case AST_MUL:
            return tacMakeBinOp(TAC_MUL, code[0], code[1]);
	    case AST_DIV:
            return tacMakeBinOp(TAC_DIV, code[0], code[1]);
	    case AST_LT:
            return tacMakeBinOp(TAC_LT, code[0], code[1]);
	    case AST_GT:
            return tacMakeBinOp(TAC_GT, code[0], code[1]);
	    case AST_LE:
            return tacMakeBinOp(TAC_LE, code[0], code[1]);
	    case AST_GE:
            return tacMakeBinOp(TAC_GE, code[0], code[1]);
	    case AST_EQ:
            return tacMakeBinOp(TAC_EQ, code[0], code[1]);
	    case AST_NE:
            return tacMakeBinOp(TAC_NE, code[0], code[1]);
	    case AST_AND:
            return tacMakeBinOp(TAC_AND, code[0], code[1]);
	    case AST_OR:
            return tacMakeBinOp(TAC_OR, code[0], code[1]);
        
        case AST_IDENT_ARR:
            return tacMakeArrAccess(node->symbol, code);

        // assignment
        case AST_ASSIGN_VAR:
            return tacMakeVarAssignment(node->symbol, code);
        case AST_ASSIGN_ARRAY:
            return tacMakeArrAssignment(node->symbol, code);

        // function declaration and parameters
        case AST_FUNCTION:
            return tacMakeFuncDefinition(node->symbol, code);
        case AST_PARAMETER:
            return tacMakeParameter(node->symbol, code);

        // control flow
        case AST_IF:
            return tacMakeIf(node->symbol, code);
        case AST_IF_ELSE:
            return tacMakeIfElse(node->symbol, code);
        case AST_WHILE:
            return tacMakeWhile(node->symbol, code);
        
        // function calls
        case AST_READ:
            return tacMakeRead(node->symbol, code);
        case AST_IDENT_FUN:
            return tacMakeFuncCall(node->symbol, code);
        case AST_EXPRESSION_LIST:
            return tacMakeArgument(node->symbol, code);

        // prints
        // the print tac is actually generated in each element of print_list,
        // so TAC_PRINT does not have to be treated
        case AST_PRINT_LIST:
            return tacMakePrint(node->symbol, code);
        case AST_PRINTABLE:
            return tacMakePrintable(node->symbol, code);

        // return
        case AST_RETURN:
            return tacMakeReturn(node->symbol, code);

        // default scenario
        default:
            return tacJoin(tacJoin(tacJoin(code[0], code[1]), code[2]), code[3]);
    }

}

TAC* tacMakeBinOp(int type, TAC* op1, TAC* op2)
{
    HASH_NODE* temp = hashCreateTemp();
    TAC* result = tacCreate(type, temp,
        op1? op1->res : NULL,
        op2? op2->res : NULL);

    return tacJoin(tacJoin(op1, op2), result);
}

TAC* tacMakeArrAccess(HASH_NODE* symbol, TAC* code[]) 
{
    HASH_NODE* temp = hashCreateTemp();
    TAC* index = code[0];
    TAC* access = tacCreate(TAC_ARR_READ, temp,
        symbol, index? index->res : NULL); 

    return tacJoin(index, access);
}

TAC* tacMakeFuncDefinition(HASH_NODE* symbol, TAC* code[])
{
    TAC* start = tacCreate(TAC_FUNC_START, symbol, NULL, NULL);
    TAC* end = tacCreate(TAC_FUNC_END, symbol, NULL, NULL);    
    TAC* type = code[0];
    TAC* params = code[1];
    TAC* body = code[2];

    return tacJoin(tacJoin(tacJoin(tacJoin(type, start), params), body), end);
}

TAC* tacMakeParameter(HASH_NODE* symbol, TAC* code[])
{
    TAC* type = code[0];
    TAC* param = tacCreate(TAC_PARAMETER, symbol, NULL, NULL);

    return tacJoin(type, param);
}

TAC* tacMakeVarAssignment(HASH_NODE* symbol, TAC* code[])
{
    TAC* value = code[0];
    TAC* assignment = tacCreate(TAC_MOV, symbol,
        value? value->res : NULL, NULL);

    return tacJoin(value, assignment);
}

TAC* tacMakeArrAssignment(HASH_NODE* symbol, TAC* code[])
{
    TAC* index = code[0];
    TAC* value = code[1];
    TAC* assignment = tacCreate(TAC_MOV_IND, symbol,
        index? index->res : NULL,
        value? value->res : NULL);

    return tacJoin(tacJoin(index, value), assignment);
}

TAC* tacMakeIf(HASH_NODE* symbol, TAC* code[])
{   
    /*
        *** GOTO JUMP_ADDR IF CONDITION IS FALSE ***

            ifz jump_addr condition
            [ifblock]
        jump_addr:
            ...
    */
    HASH_NODE* _jumpAddr = hashCreateLabel();
    TAC* jumpAddr = tacCreate(TAC_LABEL, _jumpAddr, NULL, NULL);    

    TAC* condition = code[0];
    TAC* command = code[1];

    TAC* ifz = tacCreate(TAC_IFZ, _jumpAddr,
        condition? condition->res : NULL, NULL);

    return tacJoin(tacJoin(tacJoin(condition, ifz), command), jumpAddr);
}

TAC* tacMakeIfElse(HASH_NODE* symbol, TAC* code[])
{
    /*
            ifz else_add condition
            [then block]
            jmp continue
        else_addr:
            [else block]
        continue:
            ...
    */

    HASH_NODE* _elseAddr = hashCreateLabel();
    TAC* elseAddr = tacCreate(TAC_LABEL, _elseAddr, NULL, NULL);

    HASH_NODE* _continueAddr = hashCreateLabel();
    TAC* continueAddr = tacCreate(TAC_LABEL, _continueAddr, NULL, NULL);

    TAC* condition = code[0];
    TAC* thenBlock = code[1];
    TAC* elseBlock = code[2];

    TAC* ifz = tacCreate(TAC_IFZ, _elseAddr,
        condition? condition->res : NULL, NULL);
    TAC* jump = tacCreate(TAC_JUMP, _continueAddr, NULL, NULL);

    return tacJoin(tacJoin(tacJoin(
        tacJoin(tacJoin(tacJoin(condition, ifz), thenBlock), 
        jump), elseAddr), elseBlock), continueAddr);
}

TAC* tacMakeWhile(HASH_NODE* symbol, TAC* code[])
{
    /*
        _whileStart:
            ifz _whileEnd condition
            [command]
            jmp _whileStart
        _whileEnd:

    */
    TAC* condition = code[0];
    TAC* command = code[1];

    HASH_NODE* _whileStart = hashCreateLabel();
    TAC* whileStart = tacCreate(TAC_LABEL, _whileStart, NULL, NULL);


    HASH_NODE* _whileEnd = hashCreateLabel();
    TAC* whileEnd = tacCreate(TAC_LABEL, _whileEnd, NULL, NULL);

    TAC* ifz = tacCreate(TAC_IFZ, _whileEnd,
        condition? condition->res : NULL, NULL);
    TAC* jmpToStart = tacCreate(TAC_JUMP, _whileStart, NULL, NULL);

    return tacJoin(tacJoin(tacJoin(
        tacJoin(tacJoin(whileStart, condition), ifz), command), jmpToStart), whileEnd);
}

TAC* tacMakeRead(HASH_NODE* symbol, TAC* code[])
{
    return tacCreate(TAC_READ, symbol, NULL, NULL);
}

TAC* tacMakePrint(HASH_NODE* symbol, TAC* code[])
{
    TAC* arg1 = code[0];
    TAC* arglist = code[1];
    TAC* argument = tacCreate(TAC_PRINT, arg1? arg1->res : NULL, NULL, NULL);

    return tacJoin(tacJoin(arg1, argument), arglist);
}

TAC* tacMakeFuncCall(HASH_NODE* symbol, TAC* code[])
{
    TAC* args = code[0];
    
    HASH_NODE* result = hashCreateTemp();
    TAC* call = tacCreate(TAC_CALL, result, symbol, NULL);

    return tacJoin(args, call);
}

TAC* tacMakeArgument(HASH_NODE* symbol, TAC* code[])
{
    TAC* arg1 = code[0];
    TAC* arglist = code[1];
    TAC* argument = tacCreate(TAC_ARG, arg1? arg1->res : NULL, NULL, NULL);

    return tacJoin(tacJoin(arg1, argument), arglist);
}

TAC* tacMakePrintable(HASH_NODE* symbol, TAC* code[]) 
{
    // if code[0], then this is a printable expression
    if (code[0] != NULL) {
        return code[0];
    } else {
        return tacCreate(TAC_SYMBOL, symbol, NULL, NULL);
    }
}

TAC* tacMakeReturn(HASH_NODE* symbol, TAC* code[])
{
    TAC* value = code[0];
    TAC* ret = tacCreate(TAC_RETURN, value? value->res : NULL, NULL, NULL);

    return tacJoin(value, ret);
}