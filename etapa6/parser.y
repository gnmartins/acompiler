/*
 *  Universidade Federal do Rio Grande do Sul
 *  Instituto de Informática
 *  INF01147 - Compiladores (2017/2)
 *
 *  Gabriel Nunes Martins
 *  Leonardo Silva Rosa
 */

%{
    #include <stdio.h>
    #include <stdlib.h>
    #include "hash.h"
    #include "ast.h"

    int getLineNumber();
    int yyerror();
    int yylex();
%}

%token KW_BYTE
%token KW_SHORT
%token KW_LONG
%token KW_FLOAT
%token KW_DOUBLE
%token KW_IF
%token KW_THEN
%token KW_ELSE
%token KW_WHILE
%token KW_FOR
%token KW_READ
%token KW_RETURN
%token KW_PRINT

%token OPERATOR_LE
%token OPERATOR_GE
%token OPERATOR_EQ
%token OPERATOR_NE
%token OPERATOR_AND
%token OPERATOR_OR

%token <symbol>TK_IDENTIFIER
%token <symbol>LIT_INTEGER
%token <symbol>LIT_REAL
%token <symbol>LIT_CHAR
%token <symbol>LIT_STRING

%token TOKEN_ERROR

%union
{
    HASH_NODE *symbol;
    AST_NODE *ast;
}

%left OPERATOR_OR 
%left OPERATOR_AND
%left OPERATOR_EQ OPERATOR_NE
%left '<' '>' OPERATOR_LE OPERATOR_GE
%left '+' '-'
%left '*' '/'

%type <ast> program
%type <ast> declaration
%type <ast> global
%type <ast> type
%type <ast> literal
%type <ast> literal_list
%type <ast> variable
%type <ast> array
%type <ast> function
%type <ast> parameter
%type <ast> parameter_list
%type <ast> block
%type <ast> command_list
%type <ast> command
%type <ast> assignment
%type <ast> read
%type <ast> printable
%type <ast> printable_list
%type <ast> print
%type <ast> return
%type <ast> control_flow
%type <ast> expression
%type <ast> expression_list

%%

root: 
    program             { root = $1; };

program:
    declaration             { $$ = astCreate(AST_PROGRAM, getLineNumber(), 0, $1, 0, 0, 0); }
    | program declaration   { $$ = astCreate(AST_PROGRAM, getLineNumber(), 0, $1, $2, 0, 0); };

declaration:          
    global ';'          { $$ = $1; } 
    | function          { $$ = $1; };

global:               
    variable            { $$ = $1; } 
    | array             { $$ = $1; };

type:                 
    KW_BYTE             { $$ = astCreate(AST_BYTE, getLineNumber(), 0, 0, 0, 0, 0); }
    | KW_SHORT          { $$ = astCreate(AST_SHORT, getLineNumber(), 0, 0, 0, 0, 0); }
    | KW_LONG           { $$ = astCreate(AST_LONG, getLineNumber(), 0, 0, 0, 0, 0); }
    | KW_FLOAT          { $$ = astCreate(AST_FLOAT, getLineNumber(), 0, 0, 0, 0, 0); }
    | KW_DOUBLE         { $$ = astCreate(AST_DOUBLE, getLineNumber(), 0, 0, 0, 0, 0); };

literal:              
    LIT_INTEGER         { $$ = astCreate(AST_SYMBOL, getLineNumber(), $1, 0, 0, 0, 0); }
    | LIT_REAL          { $$ = astCreate(AST_SYMBOL, getLineNumber(), $1, 0, 0, 0, 0); }
    | LIT_CHAR          { $$ = astCreate(AST_SYMBOL, getLineNumber(), $1, 0, 0, 0, 0); }
    // esses três faz sentido mas não tá especificado claramente
    | '-' LIT_INTEGER   { $$ = astCreate(AST_SYMBOL_NEGATED, getLineNumber(), $2, 0, 0, 0, 0); }
    | '-' LIT_REAL      { $$ = astCreate(AST_SYMBOL_NEGATED, getLineNumber(), $2, 0, 0, 0, 0); }
    | '-' LIT_CHAR      { $$ = astCreate(AST_SYMBOL_NEGATED, getLineNumber(), $2, 0, 0, 0, 0); };

literal_list:         
    literal                 { $$ = astCreate(AST_LITERAL_LIST, getLineNumber(), 0, $1, 0, 0, 0); }
    | literal literal_list  { $$ = astCreate(AST_LITERAL_LIST, getLineNumber(), 0, $1, $2, 0, 0); };

variable:             
    TK_IDENTIFIER ':' type '=' literal  { $$ = astCreate(AST_VAR, getLineNumber(), $1, $3, $5, 0, 0); };

array:                
    TK_IDENTIFIER ':' type '[' LIT_INTEGER ']'
            { $$ = astCreate(AST_ARRAY, getLineNumber(), $1, $3, astCreate(AST_LIT_INT, getLineNumber(), $5, 0, 0, 0, 0), 0, 0); }

    | TK_IDENTIFIER ':' type '[' LIT_INTEGER ']' literal_list
            { $$ = astCreate(AST_ARRAY, getLineNumber(), $1, $3, astCreate(AST_LIT_INT, getLineNumber(), $5, 0, 0, 0, 0), $7, 0); };

function:             
    '(' type ')' TK_IDENTIFIER '(' ')' block
            { $$ = astCreate(AST_FUNCTION, getLineNumber(), $4, $2, 0, $7, 0); }

    | '(' type ')' TK_IDENTIFIER '(' parameter_list ')' block
            { $$ = astCreate(AST_FUNCTION, getLineNumber(), $4, $2, $6, $8, 0); };

parameter:            
    TK_IDENTIFIER ':' type  { $$ = astCreate(AST_PARAMETER, getLineNumber(), $1, $3, 0, 0, 0); };

parameter_list:       
    parameter                       { $$ = astCreate(AST_PARAMETER_LIST, getLineNumber(), 0, $1, 0, 0, 0); }
    | parameter ',' parameter_list  { $$ = astCreate(AST_PARAMETER_LIST, getLineNumber(), 0, $1, $3, 0, 0); };

block:                
    '{' command_list '}'    { $$ = astCreate(AST_COMMANDS_BLOCK, getLineNumber(), 0, $2, 0, 0, 0); };

command_list:         
    command                     { $$ = astCreate(AST_COMMANDS_LIST, getLineNumber(), 0, $1, 0, 0, 0); } 
    | command ';' command_list  { $$ = astCreate(AST_COMMANDS_LIST, getLineNumber(), 0, $1, $3, 0, 0); };

command:              
    assignment          { $$ = $1; }
    | read              { $$ = $1; }
    | print             { $$ = $1; }
    | return            { $$ = $1; }
    | control_flow      { $$ = $1; }
    | block             { $$ = $1; }
    |                   { $$ = 0; };

assignment:           
    TK_IDENTIFIER '=' expression
            { $$ = astCreate(AST_ASSIGN_VAR, getLineNumber(), $1, $3, 0, 0, 0); }

    | TK_IDENTIFIER '[' expression ']' '=' expression
            { $$ = astCreate(AST_ASSIGN_ARRAY, getLineNumber(), $1, $3, $6, 0, 0); };

read:                 
    KW_READ '>' TK_IDENTIFIER       { $$ = astCreate(AST_READ, getLineNumber(), $3, 0, 0, 0, 0); };

printable:            
    expression                      { $$ = astCreate(AST_PRINTABLE, getLineNumber(), 0, $1, 0, 0, 0); }
    | LIT_STRING                    { $$ = astCreate(AST_PRINTABLE, getLineNumber(), $1, 0, 0, 0, 0); };

printable_list:       
    printable                       { $$ = astCreate(AST_PRINT_LIST, getLineNumber(), 0, $1, 0, 0, 0); }
    | printable ',' printable_list  { $$ = astCreate(AST_PRINT_LIST, getLineNumber(), 0, $1, $3, 0, 0); };

print:                
    KW_PRINT printable_list         { $$ = astCreate(AST_PRINT, getLineNumber(), 0, $2, 0, 0, 0); };

return:               
    KW_RETURN expression            { $$ = astCreate(AST_RETURN, getLineNumber(), 0, $2, 0, 0, 0); };

control_flow:         
    KW_IF '(' expression ')' KW_THEN command        
            { $$ = astCreate(AST_IF, getLineNumber(), 0, $3, $6, 0, 0); }

    | KW_IF '(' expression ')' KW_THEN command KW_ELSE command
            { $$ = astCreate(AST_IF_ELSE, getLineNumber(), 0, $3, $6, $8, 0); }

    | KW_WHILE '(' expression ')' command
            { $$ = astCreate(AST_WHILE, getLineNumber(), 0, $3, $5, 0, 0); };

expression:           
    literal                                 { $$ = $1; }
    | TK_IDENTIFIER                         { $$ = astCreate(AST_IDENT, getLineNumber(), $1, 0, 0, 0, 0); }
    | TK_IDENTIFIER '[' expression ']'      { $$ = astCreate(AST_IDENT_ARR, getLineNumber(), $1, $3, 0, 0, 0); }
    | '(' expression ')'                    { $$ = astCreate(AST_PARENTHESES, getLineNumber(), 0, $2, 0, 0, 0); }
    | expression '+' expression             { $$ = astCreate(AST_ADD, getLineNumber(), 0, $1, $3, 0, 0); }
    | expression '-' expression             { $$ = astCreate(AST_SUB, getLineNumber(), 0, $1, $3, 0, 0); }
    | expression '*' expression             { $$ = astCreate(AST_MUL, getLineNumber(), 0, $1, $3, 0, 0); }
    | expression '/' expression             { $$ = astCreate(AST_DIV, getLineNumber(), 0, $1, $3, 0, 0); }
    | expression '<' expression             { $$ = astCreate(AST_LT, getLineNumber(), 0, $1, $3, 0, 0); }
    | expression '>' expression             { $$ = astCreate(AST_GT, getLineNumber(), 0, $1, $3, 0, 0); }
    | expression OPERATOR_LE expression     { $$ = astCreate(AST_LE, getLineNumber(), 0, $1, $3, 0, 0); }
    | expression OPERATOR_GE expression     { $$ = astCreate(AST_GE, getLineNumber(), 0, $1, $3, 0, 0); }
    | expression OPERATOR_EQ expression     { $$ = astCreate(AST_EQ, getLineNumber(), 0, $1, $3, 0, 0); }
    | expression OPERATOR_NE expression     { $$ = astCreate(AST_NE, getLineNumber(), 0, $1, $3, 0, 0); }
    | expression OPERATOR_AND expression    { $$ = astCreate(AST_AND, getLineNumber(), 0, $1, $3, 0, 0); }               
    | expression OPERATOR_OR expression     { $$ = astCreate(AST_OR, getLineNumber(), 0, $1, $3, 0, 0); }
    //| expression '!' expression               
    | TK_IDENTIFIER '(' ')'                 { $$ = astCreate(AST_IDENT_FUN, getLineNumber(), $1, 0, 0, 0, 0); }
    | TK_IDENTIFIER '(' expression_list ')' { $$ = astCreate(AST_IDENT_FUN, getLineNumber(), $1, $3, 0, 0, 0); }             
    // esses dois faz sentido mas não tá especificado claramente
    | '!' '(' expression ')'                { $$ = astCreate(AST_INVERT, getLineNumber(), 0, $3, 0, 0, 0); }
    | '-' '(' expression ')'                { $$ = astCreate(AST_NEGATE, getLineNumber(), 0, $3, 0, 0, 0); };

expression_list:      
    expression                          { $$ = astCreate(AST_EXPRESSION_LIST, getLineNumber(), 0, $1, 0, 0, 0); }
    | expression ',' expression_list    { $$ = astCreate(AST_EXPRESSION_LIST, getLineNumber(), 0, $1, $3, 0, 0); };


%%

int yyerror() {
    fprintf(stderr, "Syntax error in line %d.\n", getLineNumber());
    exit(3);
}