/*
*  Universidade Federal do Rio Grande do Sul
*  Instituto de Informática
*  INF01147 - Compiladores (2017/2)
*
*  Gabriel Nunes Martins
*  Leonardo Silva Rosa
*/

#ifndef GENCO_H
#define GENCO_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hash.h"
#include "tac.h"
#include "semantic.h"

#define MAX_STRINGS 64

void tac2asm(FILE* output, char* filename, TAC* tacList);


FILE* out;

#endif