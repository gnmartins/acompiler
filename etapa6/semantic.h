/*
 *  Universidade Federal do Rio Grande do Sul
 *  Instituto de Informática
 *  INF01147 - Compiladores (2017/2)
 *
 *  Gabriel Nunes Martins
 *  Leonardo Silva Rosa
 */

#ifndef SEMANTIC_H
#define SEMANTIC_H

#include "ast.h"

typedef struct func_list {
    AST_NODE* node;
    struct func_list* next;
} FUNC_LIST;
void addFunction(AST_NODE* node);
void printFunctions();

int semanticCheck(AST_NODE* root);
void undefinedSymbolsCheck(AST_NODE* node);
void usageCheck(AST_NODE* node);
void dataTypeCheck(AST_NODE* node);

void declarationCheck(AST_NODE* node);
void setSymbolType(AST_NODE* node);
void setSymbolDataType(AST_NODE* node);

int getArithmeticType(int type1, int type2);
int typeCompatibilityCheck(int type1, int type2);
int integerTypeCheck(int type);

int funcArgumentsCheck(AST_NODE* funcCall);
AST_NODE* findFuncDeclaration(AST_NODE* funcCall);

#endif