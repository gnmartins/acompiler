/*
*  Universidade Federal do Rio Grande do Sul
*  Instituto de Informática
*  INF01147 - Compiladores (2017/2)
*
*  Gabriel Nunes Martins
*  Leonardo Silva Rosa
*/

#include <stdio.h>
#include <stdlib.h>

#include "hash.h"
#include "ast.h"
#include "semantic.h"
#include "tac.h"
#include "genco.h"

#include "lex.yy.h"
#include "y.tab.h"

extern AST_NODE *root;

int yyparse();

int main(int argc, char* argv[]) 
{

    if (argc < 3) {
        fprintf(stderr, "Missing arguments.\n");
        fprintf(stderr, "\t./etapa5 <input.txt> <output.s>\n");
        exit(1);
    }

    yyin = fopen(argv[1], "r");

    if (!yyin) {
        fprintf(stderr, "Cannot open file - %s\n", argv[1]);
        exit(2);
    }
    
    /* Syntax check */
    yyparse();
    fprintf(stderr, "Syntax check finished successfully!\n");

    /* Semantic check */
    int errCount;
    if ((errCount = semanticCheck(root)) > 0) {
        fprintf(stderr, "Found %d semantic errors.\n", errCount);
        fprintf(stderr, "Semantic check failed!\n");
        exit(4);
    }
    fprintf(stderr, "Semantic check finished successfully!\n");

    /* TAC generation */
    TAC* tac = tacGenerate(root);
    TAC* rev = tacReverseList(tac);
    tacPrintList(rev);

    // hashPrint();
    
    /* ASM code generation */
    FILE* output = stdout;
    output = fopen(argv[2], "w");
    if (!output) {
        fprintf(stderr, "Cannot open file - %s\n", argv[2]);
        exit(2);
    }
    tac2asm(output, argv[1], rev);

    fprintf(stderr, "Output success!\n");
    fclose(output);
    exit(0);

}